#!/bin/bash

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
TEXT="Новый пуш!%0AПроект:+Панель администратора+($CI_PROJECT_NAME)%0AURL:+$CI_PROJECT_URL%0ABranch:+$CI_COMMIT_REF_SLUG%0A%0AПовод:+$CI_COMMIT_TITLE"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_USER_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null