import 'dart:ui';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

BaseStyle styles = BaseStyle();

class Common {
  static const uuid = Uuid();
  static const Duration animationDuration = Duration(milliseconds: 300);
}

class ThemeColors {
  /// Акценты
  /// основной цвет бренда
  Color get brand => Colors.blue;

  /// затемнённый цвет бренда, для градиентов
  Color get brandDarker => const Color(0xff3400C8);

  /// Акцент, сообщающий о чём-то хорошем, а также символ активности
  /// FCD400
  Color get accent => const Color(0xff00D200);

  /// Акцент, сообщающий о проблемах
  Color get danger => const Color(0xffFF3E1D);

  Color get dangerDark => const Color.fromARGB(255, 164, 19, 14);

  /// Цвет важных кнопок
  Color get actionMain => Colors.blue;

  /// Цвет дополнительных кнопок
  Color get actionSecond => Colors.blue.shade900;

  /// 0174FC
  Color get thirdAccent => const Color(0xff38023D);

  /// Фоны
  /// Основной фон
  Color get background => Colors.white;

  Color get lightBackground => Colors.blue.withOpacity(0.3);

  /// Фон для всплывающих меню и т.п.
  Color get translucent => background;

  /// Фон из смеси основного фона и основного цвета бренда
  Color translucentBrand([double opacity = 0.75]) => Color.alphaBlend(
      styles.themeColors.brand.withOpacity(opacity), Colors.white);

  /// Фон для карточек
  Color get cardBackground => Colors.grey.shade400;

  /// Фон для карточек на карточках
  Color get cardSecondBackground => Colors.grey.shade500;

  /// Тексты
  /// Основной цвет текста
  Color get text => Colors.black87;

  /// Цвет текста в стиле брендинга (для ссылок, некоторых заголовков и акцентных подписей)
  Color get brandedText => brand;

  /// Инвертированный цвет текста (для текста на цвете текста)
  Color get invertedText => Colors.white;

  /// 323232
  /// Менее важный цвет текста
  Color get textSecondary => Colors.black54;

  /// Цвет подписей
  Color get caption => Colors.grey.shade700;

  Color get onBrandText => Colors.white;

  Color get onDarkBrand => Colors.white;

  Color get onAccentText => Colors.white;

  Color get onDangerText => Colors.white;

  Color get onActionMainText => Colors.white;

  Color get onActionSecondText => Colors.white;
}

class BaseStyle {
  ThemeColors get themeColors => ThemeColors();

  Center get loadingAnimation => const Center(
      child: SizedBox.square(
          dimension: 150,
          child: FlareActor("flare/loading.flr", animation: "active")));

  BoxDecoration get plateDecoration => BoxDecoration(
      gradient: LinearGradient(
          colors: [themeColors.thirdAccent, themeColors.brandDarker],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          tileMode: TileMode.clamp));

  BoxDecoration get panelDecoration => BoxDecoration(
      gradient: LinearGradient(
          colors: [themeColors.thirdAccent, themeColors.brandDarker],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          tileMode: TileMode.clamp));

  TextStyle get linkStyle => TextStyle(color: themeColors.brandedText);

  TextStyle get selectTextStyle => TextStyle(color: themeColors.brandedText);

  Widget get defaultAvatar => Image.asset("avatars/robot.png");

  ButtonStyle get wideButtonStyle => ElevatedButton.styleFrom(
        minimumSize: const Size(double.infinity, 45),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      );

  ButtonStyle get lightButtonStyle => ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(21)),
      padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 5),
      minimumSize: const Size(150, 35),
      primary: themeColors.actionMain,
      onPrimary: themeColors.brandDarker);

  ButtonStyle get brandedOutlinedButtonStyle => OutlinedButton.styleFrom(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(21)),
      primary: themeColors.brandedText,
      padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 5),
      minimumSize: const Size(150, 35),
      side: BorderSide(width: 1, color: styles.themeColors.brand));

  TextTheme get mainTextTheme => TextTheme(
        headline1: const TextStyle(
          fontFamily: "Inter",
          fontSize: 70,
          fontWeight: FontWeight.w300,
          letterSpacing: 3,
        ),
        headline2: const TextStyle(
          fontFamily: "Inter",
          fontSize: 63,
          fontWeight: FontWeight.w100,
          letterSpacing: -0.5,
        ),
        headline3: TextStyle(
            fontFamily: "Inter",
            fontSize: 28,
            fontWeight: FontWeight.w600,
            color: themeColors.caption),
        headline4: const TextStyle(
          fontFamily: "Inter",
          fontSize: 28,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.3,
        ),
        headline5: const TextStyle(
            fontFamily: "Inter",
            fontSize: 20,
            fontWeight: FontWeight.w100,
            letterSpacing: 0.3),
        headline6: const TextStyle(
            fontFamily: "Inter",
            fontSize: 18,
            fontWeight: FontWeight.w300,
            letterSpacing: 0.5),
        subtitle1: const TextStyle(
            fontFamily: "Inter",
            fontSize: 18,
            fontWeight: FontWeight.w600,
            letterSpacing: 0),
        subtitle2: const TextStyle(
            fontFamily: "Inter",
            fontSize: 15,
            fontWeight: FontWeight.w400,
            letterSpacing: 0),
        bodyText1: const TextStyle(
            fontFamily: "Inter",
            fontSize: 15,
            fontWeight: FontWeight.w400,
            letterSpacing: 0),
        bodyText2: const TextStyle(
            fontFamily: "Inter",
            fontSize: 15,
            fontWeight: FontWeight.w500,
            letterSpacing: 0),
        button: const TextStyle(
            fontFamily: "Inter",
            fontSize: 14,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.3),
        caption: TextStyle(
            fontFamily: "Inter",
            fontSize: 13,
            fontWeight: FontWeight.w500,
            letterSpacing: 0,
            color: themeColors.caption),
        overline: const TextStyle(
            fontFamily: "Inter",
            fontSize: 13,
            fontWeight: FontWeight.w600,
            letterSpacing: 0),
      );

  ThemeData get mainTheme => ThemeData(
      colorScheme: ColorScheme.fromSwatch(
              brightness: Brightness.light, primarySwatch: Colors.blue)
          .copyWith(secondary: themeColors.cardSecondBackground),
      backgroundColor: themeColors.background,
      scaffoldBackgroundColor: themeColors.background,
      primaryColor: themeColors.brand,
      primaryColorDark: themeColors.brandDarker,
      canvasColor: themeColors.background.withOpacity(0.75),
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: themeColors.brand,
        selectionColor: Colors.white24,
        selectionHandleColor: Colors.amber.shade800,
      ),
      dialogBackgroundColor: themeColors.background,
      cardColor: themeColors.cardBackground,
      toggleableActiveColor: Colors.amber.withAlpha(150),
      pageTransitionsTheme: const PageTransitionsTheme(builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      }),
      iconTheme: IconThemeData(
        color: themeColors.caption,
      ),
      textTheme: mainTextTheme,
      dialogTheme: DialogTheme(
        contentTextStyle: mainTextTheme.subtitle1!.copyWith(color: themeColors.text),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(21))),
      cardTheme: CardTheme(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(21)),
      ),

      /// SnackBar
      snackBarTheme: SnackBarThemeData(
        elevation: 0,
        behavior: SnackBarBehavior.floating,
        backgroundColor: themeColors.textSecondary,
        contentTextStyle: TextStyle(
            fontFamily: "Inter",
            fontSize: 13,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
            color: themeColors.background),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),

      /// Поля ввода
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: TextStyle(
            fontFamily: "Inter",
            fontSize: 18,
            fontWeight: FontWeight.w400,
            letterSpacing: 0,
            color: themeColors.caption),
        labelStyle: TextStyle(
            fontFamily: "Inter",
            fontSize: 15,
            fontWeight: FontWeight.w500,
            color: themeColors.brand),
        suffixStyle: const TextStyle(
          fontFamily: "Inter",
          fontSize: 18,
          fontWeight: FontWeight.w600,
          letterSpacing: 0,
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: themeColors.brand)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: themeColors.brandDarker)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: themeColors.brand)),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: themeColors.danger)),
        disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide:
                BorderSide(color: themeColors.caption.withOpacity(0.5))),
      ),

      /// AppBar
      appBarTheme: AppBarTheme(
        backgroundColor: themeColors.background.withOpacity(0.9),
        foregroundColor: themeColors.text,
        elevation: 0,
        titleTextStyle: TextStyle(
          fontFamily: "Inter",
          fontSize: 18,
          color: themeColors.onBrandText,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.5,
        ),
      ),
      checkboxTheme: CheckboxThemeData(
        fillColor: MaterialStateProperty.all(themeColors.brand),
        checkColor: MaterialStateProperty.all(themeColors.onBrandText),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        side: BorderSide(width: 2, color: themeColors.caption),
      ),

      /// кнопки
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
        primary: themeColors.brand,
        onPrimary: themeColors.onBrandText,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        minimumSize: const Size(150, 35),
        textStyle: mainTextTheme.button,
      )),
      textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
        primary: themeColors.actionMain,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        padding: EdgeInsets.zero,
        textStyle: mainTextTheme.button,
      )),
      outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
        primary: themeColors.brand,
        textStyle: mainTextTheme.button,
        side: BorderSide(width: 1, color: themeColors.brand),
        padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 16),
        minimumSize: const Size(150, 40),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      )));
}
