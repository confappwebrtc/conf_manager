import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/conf/search.dart';
import 'package:manager/generated/proto/conference.pb.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/schedule_provider.dart';
import 'package:manager/providers/search_provider.dart';
import 'package:manager/providers/sections_provider.dart';
import 'package:manager/styles.dart';
import 'package:manager/widgets/block_async.dart';

class AddSection extends ConsumerStatefulWidget {
  final String confId;

  const AddSection(this.confId, {Key? key}) : super(key: key);

  @override
  _AddSectionState createState() => _AddSectionState();
}

class _AddSectionState extends ConsumerState<AddSection> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();

  Section localSection = Section();
  String sectionId = "";

  @override
  Widget build(BuildContext context) {
    final list = ref.watch(sectionsProvider(widget.confId)).value ?? [];
    final manager = ref.watch(sectionManagerProvider);

    if (sectionId != localSection.id) {
      sectionId = localSection.id;

      _titleController.text = localSection.title;
      _descController.text = localSection.description;
    }

    submit() async {
      if (_formKey.currentState?.validate() ?? false) {
        localSection.id = Common.uuid.v4();
        localSection.conferenceId = widget.confId;
        _formKey.currentState?.save();
        final res = await blockUiOnAsyncCode(context, ()async {
          if (list.where((el) => el.id == localSection.id).isNotEmpty) {
            return await manager.update(localSection);
          } else {
            return await manager.create(localSection);
          }
        });
        if (res == true) {
          ref.refresh(allSectionsProvider);
          localSection = Section();
          _titleController.clear();
          _descController.clear();
        }
      }
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text("Sections", style: Theme.of(context).textTheme.subtitle1),
        ),
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
              flex: 2,
              child: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: Column(children: [
                    /// Title
                    TextFormField(
                        controller: _titleController,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        decoration: const InputDecoration(
                            labelText: "Title",
                            hintText: "Enter section title..."),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return "Enter title";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          if (value != null) {
                            localSection.title = value;
                          }
                        }),
                    const SizedBox(height: 15),

                    /// description
                    TextFormField(
                        minLines: 1,
                        maxLines: 1,
                        controller: _descController,
                        maxLength: 100,
                        maxLengthEnforcement: MaxLengthEnforcement.enforced,
                        decoration: const InputDecoration(
                            labelText: "About section",
                            hintText: "Enter some optional information..."),
                        validator: (value) {
                          if ((value?.isNotEmpty ?? false) &&
                              ((value?.length ?? 0) < 100)) {
                            return "Please, enter more information";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          if (value != null) {
                            localSection.description = value;
                          }
                        }),
                  ]))),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Material(
                color: styles.themeColors.brand,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  // side: BorderSide(color: styles.themeColors.brand)
                ),
                child: InkWell(
                    onTap: () async {
                      await submit();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Icon(Icons.add_rounded,
                            size: 36, color: styles.themeColors.onBrandText),
                        const SizedBox(height: 4),
                        Text("Add section",
                            style: Theme.of(context).textTheme.button!.copyWith(
                                color: styles.themeColors.onBrandText))
                      ]),
                    ))),
          ),
          Expanded(
              child: Wrap(children: [
            for (final el in list) ...[
              Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ActionChip(
                    elevation: 0,
                    label: Text(el.title,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: styles.themeColors.onDarkBrand)),
                    backgroundColor: Colors
                        .primaries[el.id.hashCode % Colors.primaries.length]
                        .shade600,
                    onPressed: () {
                      if (mounted) {
                        setState(() {
                          localSection = el;
                        });
                      }
                    },
                  ))
            ]
          ]))
        ]),
      ],
    );
  }
}

class FindSection extends ConsumerStatefulWidget {
  const FindSection({Key? key}) : super(key: key);

  @override
  ConsumerState<FindSection> createState() => _FindSectionState();
}

class _FindSectionState extends ConsumerState<FindSection> {
  final Set<Section> sections = {};

  @override
  Widget build(BuildContext context) {
    final listProvider =
        ref.watch(findSectionProvider(ref.watch(selectedConference).id));

    return Scaffold(
      backgroundColor: styles.themeColors.background,
      appBar: AppBar(title: const Text("Select sections")),
      persistentFooterButtons: [
        ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop(sections);
            },
            child: const Text("Accept")),
        const SizedBox(width: 25),
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text("Cancel"))
      ],
      body: CustomScrollView(slivers: [
        const SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsets.only(bottom: 12.0),
                child: SearchField(SearchType.section))),
        listProvider.when(
            data: (list) {
              return SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                final section = list?[index];

                if (section == null) {
                  return const SizedBox.shrink();
                }

                return Padding(
                  padding: const EdgeInsets.only(bottom: 12.0),
                  child: CheckboxListTile(
                    title: Text(section.title),
                    value: sections.contains(section),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: styles.themeColors.brand)),
                    onChanged: (value) {
                      setState(() {
                        if (value ?? false) {
                          sections.add(section);
                        } else {
                          sections.remove(section);
                        }
                      });
                    },
                  ),
                );
              }, childCount: list?.length ?? 0));
            },
            error: (e, s) => SliverToBoxAdapter(child: Text(e.toString())),
            loading: () => const SliverToBoxAdapter())
      ]),
    );
  }
}
