import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:manager/guests/add_speaker.dart';
import 'package:manager/layout.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/styles.dart';

import '../generated/proto/conference.pb.dart';
import '../providers/conf_provider.dart';
import 'add_conf.dart';

class ConfList extends ConsumerStatefulWidget {
  const ConfList({Key? key}) : super(key: key);

  @override
  _ConfListState createState() => _ConfListState();
}

class _ConfListState extends ConsumerState<ConfList> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final provider = ref.watch(conferenceListProvider);
    final _selectedIndex = ref.watch(currentPageIndexProvider.state);
    final conf = ref.watch(selectedConference.state);

    return Scaffold(
      key: _key,
      drawerScrimColor: Colors.transparent,
      endDrawer: Container(
        padding: const EdgeInsets.all(15.0),
        width: max(MediaQuery.of(context).size.width * 0.25, 300),
        decoration: BoxDecoration(
            color: styles.themeColors.background,
            border: Border(
                left: BorderSide(width: 2, color: styles.themeColors.brand))),
        child: ListView(children: [
          Text("Conference", style: Theme.of(context).textTheme.overline),
          const SizedBox(height: 10),
          Text(conf.state.title, style: Theme.of(context).textTheme.subtitle1),
          const SizedBox(height: 25),
          Text("Dates", style: Theme.of(context).textTheme.overline),
          const SizedBox(height: 10),
          Text(
              DateFormat.yMMMd().format(DateTimeSeconds.fromSecondsSinceEpoch(
                      conf.state.starts)) +
                  " - " +
                  DateFormat.yMMMd().format(
                      DateTimeSeconds.fromSecondsSinceEpoch(
                          conf.state.starts + conf.state.duration)),
              style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(height: 25),
          Text("Topic", style: Theme.of(context).textTheme.overline),
          const SizedBox(height: 10),
          Text(conf.state.topic, style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(height: 25),
          Text("Description", style: Theme.of(context).textTheme.overline),
          const SizedBox(height: 10),
          Text(conf.state.description,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption),
          const SizedBox(height: 25),
          Text("Quick actions", style: Theme.of(context).textTheme.overline),
          const SizedBox(height: 15),
          OutlinedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AddConf(id: conf.state.id)));
              },
              child: const Text("Manage Conference")),
          const SizedBox(height: 10),
          OutlinedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const AddSpeaker()));
              },
              child: const Text("Add Speaker")),
          const SizedBox(height: 10),
          OutlinedButton(
              onPressed: () {
                _selectedIndex.state = 1;
              },
              child: const Text("Edit Schedule")),
          const SizedBox(height: 10),
          OutlinedButton(
              onPressed: () {
                _selectedIndex.state = 2;
              },
              child: const Text("Manage Guests")),
          const SizedBox(height: 10),
          OutlinedButton(
              onPressed: () {
                _selectedIndex.state = 3;
              },
              child: const Text("Manage Contents")),
        ]),
      ),
      body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15)
              .copyWith(left: 15),
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text("Conferences", style: Theme.of(context).textTheme.subtitle1),
              const SizedBox(width: 15),
              ElevatedButton.icon(
                  onPressed: () {
                    /// add conference
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AddConf()));
                  },
                  label: const Text("Add conference"),
                  icon: const Icon(Icons.add_rounded))
            ]),
            provider.when(
                data: (confList) {
                  return Wrap(
                      children: confList.map((element) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: styles.themeColors.brand.withOpacity(
                              ((element.id == conf.state.id) &&
                                      conf.state.id.isNotEmpty)
                                  ? 0.3
                                  : 0)),
                      child: ConferenceTile(element, onPressed: () {
                        setState(() {
                          conf.state = element;
                        });
                        _key.currentState!.openEndDrawer();
                      }),
                    );
                  }).toList());
                },
                error: (e, s) => Text(e.toString()),
                loading: () => SizedBox.square(
                    dimension: 150, child: styles.loadingAnimation))
          ]),
    );
  }
}

class ConferenceTile extends ConsumerWidget {
  final Conference conf;
  final VoidCallback onPressed;

  const ConferenceTile(this.conf, {Key? key, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Material(
        color: styles.themeColors.brand.withOpacity(0),
        borderRadius: BorderRadius.circular(15),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
            onTap: onPressed,
            child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          height: 200,
                          width: 300,
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: styles.themeColors
                                  .brand,
                              border: Border.all(
                                  color: styles.themeColors.brand, width: 2)),
                          child: Container(
                              decoration: conf.image.isNotEmpty ? BoxDecoration(
                                  image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: MemoryImage(
                                              Uint8List.fromList(conf.image))),
                                  borderRadius: BorderRadius.circular(8)
                                  ) : null)),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(conf.title,
                            style: Theme.of(context).textTheme.subtitle1),
                      )
                    ]))));
  }
}
