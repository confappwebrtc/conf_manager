import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/schedule_provider.dart';
import 'package:sliver_tools/sliver_tools.dart';

import 'package:manager/providers/common.dart';
import '../generated/proto/conference.pb.dart';
import '../generated/proto/user.pb.dart';
import '../styles.dart';
import 'add_panel.dart';

class Schedule extends ConsumerStatefulWidget {
  const Schedule({Key? key}) : super(key: key);

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends ConsumerState<Schedule> {
  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final groupsProvider = ref.watch(groupedSchedule(conf));

    return groupsProvider.when(
        error: (e, s) => Center(
                child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "$e", style: Theme.of(context).textTheme.bodyText1),
              TextSpan(text: "$s", style: Theme.of(context).textTheme.caption),
            ]))),
        loading: () => styles.loadingAnimation,
        data: (groups) {
          return CustomScrollView(slivers: [
            for (final entry in groups.entries) ...[
              MultiSliver(pushPinnedChildren: true, children: [
                SliverAppBar(
                    elevation: 0,
                    pinned: true,
                    backgroundColor:
                        styles.themeColors.background.withOpacity(0.75),
                    title: Row(children: [
                      Text("Day ${entry.key + 1}",
                          style: Theme.of(context).textTheme.headline5),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.arrow_forward_ios_rounded,
                              size: 24, color: styles.themeColors.text)),
                      Text(
                          DateFormat.yMMMd().format(
                              DateTimeSeconds.fromSecondsSinceEpoch(
                                      conf.starts.toInt())
                                  .add(Duration(days: entry.key))),
                          style: Theme.of(context).textTheme.headline5),
                      const Spacer(),
                      ElevatedButton.icon(
                          onPressed: () async {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const AddPanel()));
                          },
                          icon: const Icon(Icons.add_rounded),
                          label: const SizedBox(
                              // width: double.infinity,
                              child: Text("Create new panel")))
                    ])),
                SliverReorderableList(
                    itemBuilder: (context, index) {
                      final item = entry.value[index];
                      final start = conf.starts.toInt() +
                          entry.value.take(index).fold<int>(
                              0,
                              (previousValue, element) =>
                                  previousValue + element.duration);

                      return Padding(
                        key: Key(item.id),
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Material(
                            color: styles.themeColors.background,
                            borderRadius: BorderRadius.circular(10),
                            clipBehavior: Clip.antiAlias,
                            child: ReorderableDragStartListener(
                              index: index,
                              child: ScheduleItemTile(item..starts = start,
                                  onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        AddPanel(id: item.id)));
                              }),
                            )),
                      );
                    },
                    itemCount: entry.value.length,
                    onReorder: (prev, next) {
                      setState(() {
                        if (prev < next) {
                          next -= 1;
                        }
                        final item = entry.value.removeAt(prev);
                        entry.value.insert(next, item);
                      });
                    }),
                SliverToBoxAdapter(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: OutlinedButton.icon(
                      onPressed: () async {
                        final panel = await showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                backgroundColor: styles.themeColors.background,
                                title: const Text("Select panel"),
                                content: const SizedBox(
                                    width: 600, child: FindPanel()),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        'Cancel',
                                        style: TextStyle(
                                            color: styles.themeColors.danger),
                                      )),
                                ],
                              );
                            });
                        setState(() {
                          /// TODO add to list
                        });
                      },
                      icon: const Icon(Icons.add_rounded),
                      label: const SizedBox(
                          // width: double.infinity,
                          child: Text("Add a panel"))),
                ))
              ])
            ]
          ]);
        });
  }
}

class ScheduleItemTile extends StatelessWidget {
  final Panel item;
  final VoidCallback? onTap;

  const ScheduleItemTile(this.item, {Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DateTime startDate =
        DateTimeSeconds.fromSecondsSinceEpoch(item.starts.toInt());
    final DateTime endDate =
        DateTimeSeconds.fromSecondsSinceEpoch(item.starts + item.duration);
    final Speaker speaker = item.user.speaker;
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: styles.themeColors.cardSecondBackground)),
        child: Row(children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(Icons.drag_handle_rounded,
                color: styles.themeColors.caption, size: 24),
          ),
          Expanded(
            child: Text(
                "${DateFormat.jm().format(startDate)} - ${DateFormat.jm().format(endDate)}"),
          ),
          Expanded(flex: 3, child: Text(item.title)),
          Expanded(
            child: Row(mainAxisSize: MainAxisSize.min, children: [
              Icon(Icons.badge_rounded, color: styles.themeColors.caption),
              Expanded(child: Text("${speaker.firstname} ${speaker.lastname}"))
            ]),
          ),
          Expanded(
            child: Row(mainAxisSize: MainAxisSize.min, children: [
              Icon(Icons.ballot_rounded, color: styles.themeColors.caption),
              Expanded(child: Text("${/*item.polls.length*/ 0} polls"))
            ]),
          ),
        ]),
      ),
    );
  }
}
