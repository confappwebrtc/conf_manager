import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:manager/conf/schedule.dart';
import 'package:manager/conf/search.dart';
import 'package:manager/generated/proto/conference.pb.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/guests/guests.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/schedule_provider.dart';
import 'package:manager/providers/search_provider.dart';
import 'package:manager/styles.dart';

class AddPanel extends ConsumerStatefulWidget {
  final String? id;

  const AddPanel({Key? key, this.id}) : super(key: key);

  @override
  _AddPanelState createState() => _AddPanelState();
}

class _AddPanelState extends ConsumerState<AddPanel> {
/*  string id = 1;
    string title = 2;
    string description = 3;
    int64 starts = 4;
    int64 ends = 5;
    Section section = 6;
    User user = 7;
    double rating = 8;
    string status = 9;*/
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();

  final ValueNotifier<Duration?> _durController =
      ValueNotifier<Duration?>(null);
  final ValueNotifier<User?> _userController = ValueNotifier<User?>(null);
  final ValueNotifier<String?> _sectionController =
      ValueNotifier<String?>(null);

  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final serverPanel =
        ref.watch(onePanelProvider(SingleItem(conf.id, widget.id ?? "")));

    Panel localPanel = Panel();

    if (localPanel.id.isEmpty && (serverPanel.value != null)) {
      localPanel = serverPanel.value ?? Panel();

      _titleController.text = localPanel.title;
      _descController.text = localPanel.description;
      _durController.value = Duration(seconds: localPanel.duration);
      _userController.value = localPanel.user;
      _sectionController.value = localPanel.sectionId;
    }

    pickDuration() async {
      await showDialog(
          context: context,
          builder: (context) {
            Duration dur = _durController.value ?? const Duration(minutes: 60);
            return AlertDialog(
              title: const Text("Choose panel duration"),
              content: SizedBox(
                width: 300,
                height: 400,
                child: FittedBox(
                  child: CupertinoTimerPicker(
                      initialTimerDuration: dur,
                      mode: CupertinoTimerPickerMode.hm,
                      onTimerDurationChanged: (Duration value) {
                        setState(() {
                          dur = value;
                        });
                      }),
                ),
              ),
              actions: [
                ElevatedButton(
                    onPressed: () {
                      if (mounted) {
                        setState(() {
                          _durController.value = dur;
                        });
                      }
                      Navigator.of(context).pop();
                    },
                    child: const Text('Submit')),
                const SizedBox(width: 25),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: styles.themeColors.danger),
                    )),
              ],
            );
          });
    }

    pickSpeaker() async {
      final user = await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              backgroundColor: styles.themeColors.background,
              title: const Text("Select speaker"),
              content: const SizedBox(width: 600, child: FindSpeaker()),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: styles.themeColors.danger),
                    )),
              ],
            );
          });
      setState(() {
        _userController.value = user;
      });
    }

    submit() {
      if (_formKey.currentState?.validate() ?? false) {
        localPanel.duration = _durController.value?.inSeconds ?? 0;
        _formKey.currentState?.save();
        log(localPanel.toDebugString());
        Navigator.of(context).pop();
      }
    }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: styles.themeColors.background.withOpacity(0.8),
          elevation: 0,
          title: Text("Add new panel",
              style: Theme.of(context).textTheme.headline6),
          actions: [
            TextButton(
                onPressed: () {
                  /// добавить
                  submit();
                },
                child: const Text("Save panel")),
          ]),
      extendBodyBehindAppBar: true,
      extendBody: true,
      bottomNavigationBar: Container(
        color: styles.themeColors.background.withOpacity(0.8),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background)),
              onPressed: () {
                /// добавить
                submit();
              },
              child: const Text("Save panel")),
        ]),
      ),
      body: Row(children: [
        Expanded(
          flex: 3,
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(padding: const EdgeInsets.only(top: 60), children: [
              /// Title
              TextFormField(
                  controller: _titleController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Title", hintText: "Enter panel title..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "Enter title";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localPanel.title = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// description
              TextFormField(
                  minLines: 5,
                  maxLines: 5,
                  controller: _descController,
                  maxLength: 500,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  decoration: const InputDecoration(
                      labelText: "About panel",
                      hintText: "Enter some optional information..."),
                  validator: (value) {
                    if ((value?.isNotEmpty ?? false) &&
                        ((value?.length ?? 0) < 100)) {
                      return "Please, enter more information";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localPanel.description = value;
                    }
                  }),

              //Duration
              Align(
                alignment: Alignment.topLeft,
                child: OutlinedButton(
                    style: styles.brandedOutlinedButtonStyle,
                    onPressed: pickDuration,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(mainAxisSize: MainAxisSize.min, children: [
                        if (_durController.value != null)
                          Text(DateFormat("hh:mm").format(
                              DateTimeSeconds.fromSecondsSinceEpoch(
                                  _durController.value?.inSeconds ?? 0,
                                  isUtc: true)))
                        else
                          const Text("Pick duration"),
                        const SizedBox(width: 15),
                        Icon(Icons.timer_rounded,
                            color: styles.themeColors.brand)
                      ]),
                    )),
              ),
              const SizedBox(height: 15),

              /// Speaker
              Align(
                alignment: Alignment.topLeft,
                child: OutlinedButton(
                    style: styles.brandedOutlinedButtonStyle,
                    onPressed: pickSpeaker,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(mainAxisSize: MainAxisSize.min, children: [
                        if (_userController.value?.id.isNotEmpty ?? false)
                          Text((_userController.value?.speaker.firstname ??
                                  "") +
                              " " +
                              (_userController.value?.speaker.lastname ?? ""))
                        else
                          const Text("Select speaker"),
                        const SizedBox(width: 15),
                        Icon(Icons.mic_rounded, color: styles.themeColors.brand)
                      ]),
                    )),
              ),
              const SizedBox(height: 15),

              const SizedBox(height: 60),
            ]),
          ),
        ),
        const SizedBox(width: 25),
        SizedBox(
            width: MediaQuery.of(context).size.width * 0.2,
            child: SafeArea(
                child: Column(children: [
              DropdownButtonFormField<PanelStatus>(
                  style: Theme.of(context).textTheme.button,
                  dropdownColor: styles.themeColors.background,
                  value: PanelStatus.values.firstWhere(
                      (element) => element.toString() == localPanel.status,
                      orElse: () => PanelStatus.created),
                  onSaved: (status) {
                    if (status == null) {
                      localPanel.status = status.toString();
                    }
                  },
                  onChanged: (status) {
                    if (status == null) {
                      localPanel.status = status.toString();
                    }
                  },
                  items: [
                    /// created, onEdit, ready, started, completed, moved, canceled
                    DropdownMenuItem(
                        child: Text("Created",
                            style: TextStyle(color: styles.themeColors.brand)),
                        value: PanelStatus.created),
                    DropdownMenuItem(
                        child: Text("Editing",
                            style: TextStyle(
                                color: styles.themeColors.brandDarker)),
                        value: PanelStatus.onEdit),
                    DropdownMenuItem(
                        child: Text("Ready",
                            style: TextStyle(color: Colors.blue.shade600)),
                        value: PanelStatus.ready),
                    DropdownMenuItem(
                        child: Text("Canceled",
                            style: TextStyle(color: styles.themeColors.danger)),
                        value: PanelStatus.canceled),
                  ])
            ])))
      ]),
    );
  }
}

class FindPanel extends ConsumerStatefulWidget {
  const FindPanel({Key? key}) : super(key: key);

  @override
  ConsumerState<FindPanel> createState() => _FindPanelState();
}

class _FindPanelState extends ConsumerState<FindPanel> {
  @override
  Widget build(BuildContext context) {
    final listProvider =
        ref.watch(findPanelProvider(ref.watch(selectedConference).id));

    return CustomScrollView(slivers: [
      const SliverToBoxAdapter(
          child: Padding(
              padding: EdgeInsets.only(bottom: 12.0),
              child: SearchField(SearchType.panel))),
      listProvider.when(
          data: (list) {
            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              final panel = list?[index];

              if (panel == null) {
                return const SizedBox.shrink();
              }

              return Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: ScheduleItemTile(
                  panel,
                  onTap: () {
                    Navigator.of(context).pop(panel);
                  },
                ),
              );
            }, childCount: list?.length ?? 0));
          },
          error: (e, s) => SliverToBoxAdapter(child: Text(e.toString())),
          loading: () => const SliverToBoxAdapter())
    ]);
  }
}
