import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:intl/intl.dart';
import 'package:manager/generated/proto/conference.pb.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/styles.dart';
import 'package:manager/widgets/block_async.dart';
import 'package:manager/widgets/success_dialog.dart';

import 'add_section.dart';

class AddConf extends ConsumerStatefulWidget {
  final String? id;

  const AddConf({Key? key, this.id}) : super(key: key);

  @override
  _AddConferenceState createState() => _AddConferenceState();
}

class _AddConferenceState extends ConsumerState<AddConf> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _topicController = TextEditingController();
  final TextEditingController _descController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _telegramController = TextEditingController();
  final TextEditingController _vkController = TextEditingController();
  final TextEditingController _instController = TextEditingController();

  final ValueNotifier<Uint8List?> _imageController =
      ValueNotifier<Uint8List?>(null);

  final ValueNotifier<DateTime?> _startsController =
      ValueNotifier<DateTime?>(null);
  final ValueNotifier<Duration?> _durController =
      ValueNotifier<Duration?>(null);

  Conference localConference = Conference();

  @override
  Widget build(BuildContext context) {
    final confManager = ref.watch(confManagerProvider);
    final serverConference = ref.watch(oneConfProvider(
        SingleItem(ref.watch(selectedConference).id, widget.id ?? "")));

    // if (widget.id?.isNotEmpty ?? false) {
    //   serverConference = ;
    // }
    // AsyncValue.data(conf);

    return serverConference.when(
        data: (conf) {
          if (localConference.id.isEmpty) {
            if ((conf != null) && conf.id.isNotEmpty) {
              localConference = conf;
              _titleController.text = localConference.title;
              _topicController.text = localConference.topic;
              _descController.text = localConference.description;
              _emailController.text = localConference.email;
              _phoneController.text = localConference.phone;
              _telegramController.text = localConference.telegram;
              _vkController.text = localConference.vk;
              _instController.text = localConference.instagram;
              _imageController.value =
                  Uint8List.fromList(localConference.image);
              _startsController.value =
                  DateTimeSeconds.fromSecondsSinceEpoch(localConference.starts);
              _durController.value =
                  Duration(seconds: localConference.duration);
            } else {
              localConference.id = Common.uuid.v4();
            }
          }

          pickDate() async {
            final range = await showDateRangePicker(
                context: context,
                builder: (context, child) {
                  if (child == null) {
                    return const SizedBox.shrink();
                  }
                  return Theme(
                    data: Theme.of(context).copyWith(
                        appBarTheme: Theme.of(context).appBarTheme.copyWith(
                            backgroundColor: styles.themeColors.brandDarker,
                            iconTheme: IconThemeData(
                                color: styles.themeColors.onDarkBrand)),
                        colorScheme: ColorScheme.light(
                            onPrimary: styles.themeColors.onDarkBrand,
                            primary: styles.themeColors.brandDarker)),
                    child: child,
                  );
                },
                cancelText: "Cancel",
                confirmText: "Confirm",
                saveText: "Save",
                errorInvalidRangeText: "Invalid range",
                initialDateRange: DateTimeRange(
                    start: _startsController.value ?? DateTime.now(),
                    end: (_startsController.value ?? DateTime.now())
                        .add(_durController.value ?? const Duration(days: 2))),
                firstDate: _startsController.value ?? DateTime.now(),
                currentDate: DateTime.now(),
                lastDate: (_startsController.value ?? DateTime.now())
                    .add(const Duration(days: 300 * 2)));
            if (mounted) {
              setState(() {
                _startsController.value = range?.start;
                _durController.value = range?.duration;
              });
            }
          }

          submit() async {
            if (_formKey.currentState?.validate() ?? false) {
              if ((_startsController.value == null) ||
                  (_durController.value == null)) {
                await pickDate();
              }
              localConference.starts =
                  _startsController.value?.secondsSinceEpoch ?? 0;
              localConference.duration = _durController.value?.inSeconds ?? 0;
              localConference.image = _imageController.value?.toList() ?? [];
              _formKey.currentState?.save();

              final res = await blockUiOnAsyncCode(context, () async {
                if (widget.id != null) {
                  return await confManager.update(localConference);
                } else {
                  return await confManager.create(localConference);
                }
              });
              if (res == true) {
                ref.refresh(conferenceListProvider);
                Navigator.of(context).pop();
                showSuccessDialog(context, message: "Saved successfully");
              }
            }
          }

          return Scaffold(
            appBar: AppBar(
                backgroundColor: styles.themeColors.background.withOpacity(0.8),
                elevation: 0,
                title: Text("Add new conference",
                    style: Theme.of(context).textTheme.headline6),
                actions: [
                  TextButton(
                      onPressed: () {
                        /// добавить
                        submit();
                        if (Navigator.of(context).canPop()) {
                          Navigator.of(context).pop();
                        }
                      },
                      child: const Text("Save conference")),
                ]),
            extendBodyBehindAppBar: true,
            extendBody: true,
            bottomNavigationBar: Container(
              color: styles.themeColors.background.withOpacity(0.8),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 20),
                        textStyle: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(color: styles.themeColors.background)),
                    onPressed: () {
                      /// добавить конференцию
                      submit();
                    },
                    child: const Text("Save conference")),
              ]),
            ),
            body: Row(children: [
              Expanded(
                flex: 3,
                child: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: ListView(
                      padding: const EdgeInsets.only(top: 60),
                      children: [
                        AddSection(localConference.id),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text("Conference",
                              style: Theme.of(context).textTheme.subtitle1),
                        ),

                        /// Title
                        TextFormField(
                            controller: _titleController,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(100)
                            ],
                            decoration: const InputDecoration(
                                labelText: "Title",
                                hintText: "Enter conference title..."),
                            validator: (value) {
                              if (value?.isEmpty ?? true) {
                                return "Enter title";
                              }
                              return null;
                            },
                            onSaved: (value) {
                              if (value != null) {
                                localConference.title = value;
                              }
                            }),
                        const SizedBox(height: 15),

                        /// Topic
                        TextFormField(
                            controller: _topicController,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(100)
                            ],
                            decoration: const InputDecoration(
                                labelText: "Topic",
                                hintText: "Enter conference topic..."),
                            validator: (value) {
                              if (value?.isEmpty ?? true) {
                                return "Enter topic";
                              }
                              return null;
                            },
                            onSaved: (value) {
                              if (value != null) {
                                localConference.topic = value;
                              }
                            }),
                        const SizedBox(height: 15),

                        Align(
                          alignment: Alignment.topLeft,
                          child: OutlinedButton(
                              style: styles.brandedOutlinedButtonStyle,
                              onPressed: pickDate,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      if ((_startsController.value != null) &&
                                          (_durController.value != null))
                                        Text(DateFormat.yMMMd().format(
                                                _startsController.value!) +
                                            " - " +
                                            DateFormat.yMMMd().format(
                                                _startsController.value!.add(
                                                    _durController.value!)) +
                                            " (${_durController.value?.inDays ?? 0} days)")
                                      else
                                        const Text("Pick date range"),
                                      const SizedBox(width: 15),
                                      Icon(Icons.calendar_today_rounded,
                                          color: styles.themeColors.brand)
                                    ]),
                              )),
                        ),
                        const SizedBox(height: 15),

                        /// description
                        TextFormField(
                            minLines: 5,
                            maxLines: 5,
                            controller: _descController,
                            maxLength: 500,
                            maxLengthEnforcement: MaxLengthEnforcement.enforced,
                            decoration: const InputDecoration(
                                labelText: "About conference",
                                hintText: "Enter some optional information..."),
                            validator: (value) {
                              if ((value?.isNotEmpty ?? false) &&
                                  ((value?.length ?? 0) < 100)) {
                                return "Please, enter more information";
                              }
                              return null;
                            },
                            onSaved: (value) {
                              if (value != null) {
                                localConference.description = value;
                              }
                            }),
                        const SizedBox(height: 15),

                        /// email
                        Row(children: [
                          Expanded(
                            child: TextFormField(
                                controller: _emailController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(30)
                                ],
                                decoration: const InputDecoration(
                                    labelText: "Email",
                                    hintText: "email@example.com"),
                                validator: (value) {
                                  if (value?.isEmpty ?? true) {
                                    return "It's necessary to enter email";
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  if (value != null) {
                                    localConference.email = value;
                                  }
                                }),
                          ),
                          const SizedBox(width: 15),

                          /// phone
                          Expanded(
                            child: TextFormField(
                                controller: _phoneController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(30)
                                ],
                                decoration: const InputDecoration(
                                    labelText: "Phone",
                                    hintText: "+79990000000"),
                                onSaved: (value) {
                                  if (value != null) {
                                    localConference.phone = value;
                                  }
                                }),
                          ),
                        ]),
                        const SizedBox(height: 15),

                        Row(
                          children: [
                            /// telegram
                            Expanded(
                              child: TextFormField(
                                  controller: _telegramController,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(30)
                                  ],
                                  decoration: const InputDecoration(
                                      labelText: "Telegram",
                                      hintText: "@example"),
                                  onSaved: (value) {
                                    if (value != null) {
                                      localConference.telegram = value;
                                    }
                                  }),
                            ),
                            const SizedBox(width: 15),

                            /// vk
                            Expanded(
                                child: TextFormField(
                                    controller: _vkController,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(30)
                                    ],
                                    decoration: const InputDecoration(
                                        labelText: "Vk", hintText: "@example"),
                                    onSaved: (value) {
                                      if (value != null) {
                                        localConference.vk = value;
                                      }
                                    })),
                            const SizedBox(width: 15),

                            /// inst
                            Expanded(
                                child: TextFormField(
                                    controller: _instController,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(30)
                                    ],
                                    decoration: const InputDecoration(
                                        labelText: "Instagram",
                                        hintText: "@example"),
                                    onSaved: (value) {
                                      if (value != null) {
                                        localConference.instagram = value;
                                      }
                                    })),
                          ],
                        ),

                        const SizedBox(height: 60),
                      ]),
                ),
              ),
              const SizedBox(width: 25),
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.2,
                  child: SafeArea(
                      child: Column(children: [
                    Container(
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: MemoryImage(Uint8List.fromList(
                                    _imageController.value ?? []))),
                            borderRadius: BorderRadius.circular(15),
                            color: styles.themeColors.brand),
                        width: MediaQuery.of(context).size.width * 0.2,
                        height: MediaQuery.of(context).size.width * 0.2,
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () async {
                                try {
                                  _imageController.value = await ImagePickerWeb
                                      .getImageAsBytes(); //.getImageAsWidget();
                                } catch (e) {
                                  log(e.toString());
                                }
                                setState(() {});
                              },
                              child: Column(children: [
                                const Spacer(),
                                Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10)
                                        .copyWith(
                                            bottom: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01),
                                    color: styles.themeColors.text,
                                    child: Center(
                                      child: Text("Change photo",
                                          style: TextStyle(
                                              color: styles
                                                  .themeColors.invertedText)),
                                    ))
                              ]),
                            ))),
                    const SizedBox(height: 25),
                    DropdownButtonFormField<ConfStatus>(
                        style: Theme.of(context).textTheme.button,
                        dropdownColor: styles.themeColors.background,
                        value: ConfStatus.values.firstWhere(
                            (element) =>
                                element.toString() == localConference.status,
                            orElse: () => ConfStatus.created),
                        onSaved: (status) {
                          if (status == null) {
                            localConference.status = status.toString();
                          }
                        },
                        onChanged: (status) {
                          if (status == null) {
                            localConference.status = status.toString();
                          }
                        },
                        items: [
                          /// created, onEdit, ready, started, completed, moved, canceled
                          DropdownMenuItem(
                              child: Text("Created",
                                  style: TextStyle(
                                      color: styles.themeColors.brand)),
                              value: ConfStatus.created),
                          DropdownMenuItem(
                              child: Text("Editing",
                                  style: TextStyle(
                                      color: styles.themeColors.brandDarker)),
                              value: ConfStatus.onEdit),
                          DropdownMenuItem(
                              child: Text("Ready",
                                  style:
                                      TextStyle(color: Colors.blue.shade600)),
                              value: ConfStatus.ready),
                          DropdownMenuItem(
                              child: Text("Started",
                                  style: TextStyle(
                                      color: styles.themeColors.accent)),
                              value: ConfStatus.started),
                          const DropdownMenuItem(
                              child: Text("Completed",
                                  style: TextStyle(color: Colors.green)),
                              value: ConfStatus.completed),
                          const DropdownMenuItem(
                              child: Text("Moved",
                                  style: TextStyle(color: Colors.deepOrange)),
                              value: ConfStatus.moved),
                          DropdownMenuItem(
                              child: Text("Canceled",
                                  style: TextStyle(
                                      color: styles.themeColors.danger)),
                              value: ConfStatus.canceled),
                        ])
                  ])))
            ]),
          );
        },
        error: (e, s) => Center(child: Text(e.toString())),
        loading: () => const SizedBox(width: 150, height: 150));
  }
}
