import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/providers/search_provider.dart';

class SearchField extends ConsumerStatefulWidget {
  final SearchType searchType;
  const SearchField(this.searchType, {Key? key}) : super(key: key);

  @override
  ConsumerState<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends ConsumerState<SearchField> {
  final searchKey = GlobalKey<FormFieldState>();

  @override
  Widget build(BuildContext context) {
    final searchQueryProvider =
        ref.watch(searchProvider(widget.searchType).state);

    return TextFormField(
      key: searchKey,
      initialValue: searchQueryProvider.state,
      decoration: InputDecoration(
          suffixIcon: IconButton(
        icon: const Icon(Icons.search_rounded),
        onPressed: () {
          searchKey.currentState?.save();
        },
      )),
      onEditingComplete: () {
        searchKey.currentState?.save();
      },
      onFieldSubmitted: (text) {
        searchKey.currentState?.save();
      },
      onSaved: (text) {
        searchQueryProvider.state = text ?? "";
      },
    );
  }
}
