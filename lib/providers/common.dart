import 'dart:async';

import 'package:flutter/foundation.dart';

class SingleItem {
  final String confId;
  final String itemId;

  SingleItem(this.confId, this.itemId);
}

extension DateTimeSeconds on DateTime {
  get secondsSinceEpoch {
    return Duration(milliseconds: millisecondsSinceEpoch).inSeconds;
  }

  static fromSecondsSinceEpoch(int seconds, {bool isUtc = false}) {
    return DateTime.fromMillisecondsSinceEpoch(
        Duration(seconds: seconds).inMilliseconds, isUtc: isUtc);
  }
}

abstract class Manager<T> {
  final String api;

  Manager(this.api);

  FutureOr<bool> create(T value);

  FutureOr<bool> update(T newValue);

  delete(String id) {}

  FutureOr<T?> getById(String id) => null;
}
