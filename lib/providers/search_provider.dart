import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/generated/proto/conference.pb.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/providers/schedule_provider.dart';
import 'package:manager/providers/sections_provider.dart';
import 'package:manager/providers/users_provider.dart';

enum SearchType { guest, speaker, panel, section }

final searchProvider =
    StateProvider.family<String, SearchType>((ref, searchType) => "");

final findPanelProvider =
    Provider.family<AsyncValue<List<Panel>?>, String>((ref, confId) {
  final searchValue = ref.watch(searchProvider(SearchType.panel));

  var res = const AsyncValue<List<Panel>?>.loading();

  ref.watch(panelsProvider(confId)).whenData((items) {
    res = AsyncValue<List<Panel>?>.data(items.where((element) {
      final inText =
          (element.title.toLowerCase().contains(searchValue.toLowerCase()) ||
              element.description
                  .toLowerCase()
                  .contains(searchValue.toLowerCase()));
      /* final inNames =
          ((element.user.speaker.firstname + element.user.speaker.lastname)
                  .toLowerCase()
                  .contains(searchValue)) ||
              (element.user.promo.title.toLowerCase().contains(searchValue)); */
      return inText;
    }).toList());
  });

  return res;
});

final findSectionProvider =
    Provider.family<AsyncValue<List<Section>?>, String>((ref, confId) {
  final searchValue = ref.watch(searchProvider(SearchType.section));

  var res = const AsyncValue<List<Section>?>.loading();

  ref.watch(sectionsProvider(confId)).whenData((items) {
    res = AsyncValue<List<Section>?>.data(items
        .where((element) =>
            element.title.toLowerCase().contains(searchValue.toLowerCase()) ||
            element.description
                .toLowerCase()
                .contains(searchValue.toLowerCase()))
        .toList());
  });

  return res;
});

final findSpeakerProvider =
    Provider.family<AsyncValue<List<User>?>, String>((ref, confId) {
  final searchValue = ref.watch(searchProvider(SearchType.speaker));

  var res = const AsyncValue<List<User>?>.loading();

  ref.watch(usersProvider(confId)).whenData((items) {
    res = AsyncValue<List<User>?>.data(items.where((element) {
      if (element.hasSpeaker() || element.hasPromo()) {
        return ((element.speaker.firstname + element.speaker.lastname)
                .toLowerCase()
                .contains(searchValue.toLowerCase()) ||
            element.promo.title
                .toLowerCase()
                .contains(searchValue.toLowerCase()));
      }
      return false;
    }).toList());
  });

  return res;
});
