import 'dart:async';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/providers/sections_provider.dart';

import '../generated/proto/conference.pb.dart';
import '../generated/proto/user.pb.dart';
import '../styles.dart';
import 'common.dart';

final mockDate = DateTime.now();

enum PanelStatus { created, onEdit, ready, canceled }

final scheduleProvider =
    StreamProvider.family<List<Panel>, String>((ref, confId) {
  final streamController = StreamController<List<Panel>>();

  streamController.add(mockScheduleList);

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final panelsProvider =
    StreamProvider.family<List<Panel>, String>((ref, confId) {
  final streamController = StreamController<List<Panel>>();

  streamController.add(mockScheduleList);

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final groupedSchedule =
    Provider.family<AsyncValue<Map<int, List<Panel>>>, Conference>((ref, conf) {
  var res = const AsyncValue<Map<int, List<Panel>>>.loading();

  ref.watch(scheduleProvider(conf.id)).whenData((items) {
    final Map<int, List<Panel>> groups = {};
    for (final element in items) {
      final res =
          Duration(seconds: (element.starts - conf.starts).toInt()).inDays;
      if (!groups.containsKey(res)) {
        groups[res] = items
            .where((element) =>
                res ==
                Duration(seconds: (element.starts - conf.starts).toInt())
                    .inDays)
            .toList();
      }
    }
    res = AsyncValue<Map<int, List<Panel>>>.data(groups);
  });
  return res;
});

final onePanelProvider =
    Provider.family<AsyncValue<Panel?>, SingleItem>((ref, item) {
  var res = const AsyncValue<Panel?>.loading();

  if (item.itemId.isEmpty) {
    return const AsyncValue<Panel?>.data(null);
  }

  ref.watch(scheduleProvider(item.confId)).whenData((items) {
    res = AsyncValue<Panel?>.data(
        items.firstWhere((element) => element.id == item.itemId));
  });

  return res;
});

final oneSectionProvider =
    Provider.family<AsyncValue<Section?>, SingleItem>((ref, item) {
  var res = const AsyncValue<Section?>.loading();

  if (item.itemId.isEmpty) {
    return const AsyncValue<Section?>.data(null);
  }

  ref.watch(sectionsProvider(item.confId)).whenData((items) {
    res = AsyncValue<Section?>.data(
        items.firstWhere((element) => element.id == item.itemId));
  });

  return res;
});

/*final mockSectionsList = [
  Section(
      id: Common.uuid.v4(),
      title: "Android",
      description: "Section for Android mobile developers"),
  Section(
      id: Common.uuid.v4(),
      title: "iOS",
      description: "Section for iOS mobile developers"),
  Section(
      id: Common.uuid.v4(),
      title: "Marketing",
      description: "Section for every marketing fan"),
];*/

final mockScheduleList = [
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day - 1, 9)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "Swift - the new language for iOS coding",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day - 1, 11)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "Objective C vs. Swift",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day - 1, 13)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "How to fall in love with iOS",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Salem", lastname: "Ajamia"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day, 9)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "Swift - the new language for iOS coding",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day, 11)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "Objective C vs. Swift",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day, 13)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "How to fall in love with iOS",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Salem", lastname: "Ajamia"))),
  Panel(
      id: Common.uuid.v4(),
      starts: DateTime(mockDate.year, mockDate.month, mockDate.day - 1, 9)
          .secondsSinceEpoch,
      duration: const Duration(minutes: 90).inSeconds,
      // durationMillis: const Duration(minutes: 90).inMilliseconds,
      title: "Swift - the new language for iOS coding",
      user: User(
          id: Common.uuid.v4(),
          speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina"))),
];
