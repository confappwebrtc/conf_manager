import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../generated/proto/conference.pb.dart';
import '../generated/proto/user.pb.dart';
import '../styles.dart';
import 'common.dart';

const guestsKey = "Guests";
const speakersKey = "Speakers";
const adminsKey = "Admins";
const promoKey = "Promo";

final User mockUser = User(
    id: Common.uuid.v4(),
    login: "example@confapp.com",
    guest: Guest(nickname: "Example User"));

enum Sex { male, female, other }

enum UserStatus { created, onEdit, active, denied }

final userProvider =
    StateProvider<User>((ref) => kDebugMode ? mockUser : User.getDefault());

final usersProvider = StreamProvider.family<List<User>, String>((ref, confId) {
  final streamController = StreamController<List<User>>();

  streamController.add(mockUserList);

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final groupedUsers =
    Provider.family<AsyncValue<Map<String, List<User>>>, Conference>(
        (ref, conf) {
  var res = const AsyncValue<Map<String, List<User>>>.loading();

  ref.watch(usersProvider(conf.id)).whenData((items) {
    final Map<String, List<User>> groups = {};
    for (final element in items) {
      String key = "";
      if (element.hasGuest()) {
        key = guestsKey;
      } else if (element.hasSpeaker()) {
        key = speakersKey;
      } else if (element.hasPromo()) {
        key = promoKey;
      } else {
        key = adminsKey;
      }
      groups[key] = [...?groups[key], element];
    }
    res = AsyncValue<Map<String, List<User>>>.data(groups);
  });
  return res;
});

final oneUserProvider =
    Provider.family<AsyncValue<User?>, SingleItem>((ref, item) {
  var res = const AsyncValue<User?>.loading();

  if (item.itemId.isEmpty) {
    return const AsyncValue<User?>.data(null);
  }

  ref.watch(usersProvider(item.confId)).whenData((items) {
    res = AsyncValue<User?>.data(
        items.firstWhere((element) => element.id == item.itemId));
  });

  return res;
});

final mockUserList = <User>[
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(
          firstname: "Tatiana",
          lastname: "Kalekina",
          nickname: "Tati",
          city: "Vladimir",
          gender: "female",
          company: "ConfApp",
          job: "Frontend Developer")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Salem", lastname: "Ajamia")),
  User(
      id: Common.uuid.v4(),
      promo: Promo(
          title: "Vladimir State Universit",
          nickname: "VlSU",
          address: "Vladimir, Gorkogo st., 87")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "it fan")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "female")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Salem", lastname: "Ajamia")),
  User(
      id: Common.uuid.v4(),
      promo: Promo(
          title: "Vladimir State Universit",
          nickname: "VlSU",
          address: "Vladimir, Gorkogo st., 87")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "it fan")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "female")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Salem", lastname: "Ajamia")),
  User(
      id: Common.uuid.v4(),
      promo: Promo(
          title: "Vladimir State Universit",
          nickname: "VlSU",
          address: "Vladimir, Gorkogo st., 87")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "it fan")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "female")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Tatiana", lastname: "Kalekina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Anastasia", lastname: "Roshchina")),
  User(
      id: Common.uuid.v4(),
      speaker: Speaker(firstname: "Salem", lastname: "Ajamia")),
  User(
      id: Common.uuid.v4(),
      promo: Promo(
          title: "Vladimir State Universit",
          nickname: "VlSU",
          address: "Vladimir, Gorkogo st., 87")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "it fan")),
  User(id: Common.uuid.v4(), guest: Guest(nickname: "female")),
];
