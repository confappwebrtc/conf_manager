import 'dart:convert';
import 'dart:developer';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:manager/generated/proto/user.pb.dart';
// import 'dart:io';

Future<bool> checkConnection() async {
  /* 
    var httpClient = HttpClient();
  var uri = Uri.http('0.0.0.0:9001', '/');
  var request = await httpClient.getUrl(uri);
  var response = await request.close();
  // var responseBody = await response.transform(UTF8.decoder).join();
  return true; */

  // final ans = await http.get(Uri(host: 'localhost', port: 9001, query: '/'));
  try {
    final ans = 
    await http.get(Uri.parse('http://0.0.0.0:9001/'));

/*    final res = await http.get(Uri.parse('http://0.0.0.0:9001/getRoles'));
    final obj = Roles.fromBuffer(base64Decode(res.body));
    log((obj).toString());*/

    if (ans.body.contains('Node')) {
      return true;
    }
  } catch (e) {
    log(e.toString());
  }
  return false;
}

final apiProvider = Provider<String>((ref) => 'http://0.0.0.0:9001');
