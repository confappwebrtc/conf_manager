import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import 'package:manager/styles.dart';

import '../generated/proto/conference.pb.dart';
import 'api.dart';
import 'common.dart';

enum ConfStatus { created, onEdit, ready, started, completed, moved, canceled }

final selectedConference = StateProvider<Conference>((ref) => Conference(
    id: "1",
    title: "test",
    starts: DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day - 1, 9)
        .secondsSinceEpoch));

mockConfData(String api) async {
  for (final conf in mockConfList) {
    try {
      final res = await http.post(Uri.parse(api + "/createConference"),
          body: {"data": base64Encode(conf.writeToBuffer())});
      log(base64Decode(res.body).toString());
    } catch (e) {
      log(e.toString());
    }
  }
}

final conferenceListProvider = StreamProvider<List<Conference>>((ref) {
  List<Conference> list = [];
  final streamController = StreamController<List<Conference>>();

/*  if (kDebugMode) {
    mockConfData(ref.read(apiProvider));
  } */

  http.get(Uri.parse(ref.read(apiProvider) + "/getConferences")).then((value) {
    list = Conferences.fromBuffer(base64Decode(value.body)).conferences;
    streamController.add(list);
  });

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final oneConfProvider =
    Provider.family<AsyncValue<Conference?>, SingleItem>((ref, item) {
  AsyncValue<Conference?> value = const AsyncLoading();


  ref.watch(conferenceListProvider).whenData((list) {
    if (item.itemId.isNotEmpty) {
      try {
        value = AsyncData(list.firstWhere((element) => element.id == item.itemId, orElse: () => Conference()));
        // while (value.value?.id.isEmpty ?? true) {
        // http.post(Uri.parse(ref.read(apiProvider) + "/getConferenceById"),
        //     body: {
        //       "data": base64Encode(Conference(id: item.itemId).writeToBuffer())
        //     }).then((res) {
        //   final conf = Conference.fromBuffer(base64Decode(res.body));
        //   value = AsyncData(conf);
        // });
        // }
      } catch (e) {
        value = AsyncError(e);
      }
    } else {
      value = const AsyncData(null);
    }
  });

  return value;
});

final confManagerProvider =
    Provider<ConfManager>((ref) => ConfManager(ref.read(apiProvider)));

class ConfManager extends Manager<Conference> {
  ConfManager(String api) : super(api);

  @override
  FutureOr<bool> create(Conference value) async {
    try {
      final res = await http.post(Uri.parse(api + "/createConference"),
          body: {"data": base64Encode(value.writeToBuffer())});
      if (res.statusCode < 300)  {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  FutureOr<bool> update(Conference newValue) async {
    try {
      final res = await http.post(Uri.parse(api + "/updateConference"),
          body: {"data": base64Encode(newValue.writeToBuffer())});
      if( res.statusCode < 300)  {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  FutureOr<Conference> getById(String id) async {
    try {
      final res = await http.post(Uri.parse(api + "/getConferenceById"),
          body: {"data": base64Encode(Conference(id: id).writeToBuffer())});
      final Conference conf = Conference.fromBuffer(base64Decode(res.body));
      return conf;
    } catch (e) {
      rethrow;
    }
  }
}

final mockConfList = [
  Conference(
    id: Common.uuid.v4(),
    title: "IT talent starter pack",
  ),
  Conference(id: Common.uuid.v4(), title: "Successful morning start"),
  Conference(id: Common.uuid.v4(), title: "New year conversations"),
];
