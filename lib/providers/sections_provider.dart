import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import 'package:manager/styles.dart';

import '../generated/proto/conference.pb.dart';
import 'api.dart';
import 'common.dart';

final sectionManagerProvider =
    Provider<SectionsManager>((ref) => SectionsManager(ref.read(apiProvider)));

final allSectionsProvider = StreamProvider<List<Section>>((ref) {
  final streamController = StreamController<List<Section>>();
  final manager = ref.watch(sectionManagerProvider);

  manager.getList().then((value) {
    streamController.add(value);
  });

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final sectionsProvider =
    Provider.family<AsyncValue<List<Section>>, String>((ref, confId) {
  AsyncValue<List<Section>> value = const AsyncLoading();

  ref.watch(allSectionsProvider).whenData((list) {
    list.retainWhere((element) => element.conferenceId == confId);
    value = AsyncData(list);
  });

/*  ref.listen(allSectionsProvider, (prev, AsyncValue<List<Section>>? list) {
    if (list != null) {
      final tmp = list.value ?? [];
      tmp.retainWhere((element) => element.conferenceId == confId);
      value = AsyncData(tmp);
    }
  });*/

  return value;
});

class SectionsManager extends Manager<Section> {
  SectionsManager(String api) : super(api);

  Future<List<Section>> getList() async {
    List<Section> list = [];

    await http.get(Uri.parse(api + "/getSections")).then((value) {
      list = Sections.fromBuffer(base64Decode(value.body)).sections;
    });

    return list;
  }

  @override
  FutureOr<bool> create(Section value) async {
    try {
      final res = await http.post(Uri.parse(api + "/createSection"),
          body: {"data": base64Encode(value.writeToBuffer())});
      if (res.statusCode < 300) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  FutureOr<bool> update(Section newValue) async {
    try {
      final res = await http.post(Uri.parse(api + "/updateSection"),
          body: {"data": base64Encode(newValue.writeToBuffer())});
      if (res.statusCode < 300) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  FutureOr<Section> getById(String id) async {
    try {
      final res = await http.post(Uri.parse(api + "/getSectionById"),
          body: {"data": base64Encode(Section(id: id).writeToBuffer())});
      final Section sections = Section.fromBuffer(base64Decode(res.body));
      return sections;
    } catch (e) {
      rethrow;
    }
  }
}
