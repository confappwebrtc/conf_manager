import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/conf/search.dart';
import 'package:manager/guests/add_guest.dart';
import 'package:manager/layout.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/search_provider.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';
import 'package:sliver_tools/sliver_tools.dart';

import 'add_promo.dart';
import 'add_speaker.dart';

class Guests extends ConsumerStatefulWidget {
  const Guests({Key? key}) : super(key: key);

  @override
  _GuestsState createState() => _GuestsState();
}

class _GuestsState extends ConsumerState<Guests> {
  @override
  Widget build(BuildContext context) {
    final groupedUsersProvider =
        ref.watch(groupedUsers(ref.watch(selectedConference)));

    return groupedUsersProvider.when(
        data: (groups) {
          return CustomScrollView(slivers: [
            for (final entry in groups.entries) ...[
              MultiSliver(pushPinnedChildren: true, children: [
                SliverPadding(
                    padding: const EdgeInsets.only(top: 20.0),
                    sliver: SliverAppBar(
                        elevation: 0,
                        pinned: true,
                        backgroundColor:
                            styles.themeColors.background.withOpacity(0.75),
                        title: Builder(builder: (context) {
                          if (entry.key == speakersKey) {
                            return Row(children: [
                              Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Icon(Icons.mic_rounded,
                                      color: styles.themeColors.text)),
                              Text(entry.key,
                                  style: Theme.of(context).textTheme.headline5),
                              const Spacer(),
                              ElevatedButton.icon(
                                  onPressed: () {
                                    /// add speaker
                                    // ref.read(currentPageProvider.state).state =
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const AddSpeaker(
                                                    key: Key("AddSpeaker"))));
                                  },
                                  icon: const Icon(Icons.add_rounded),
                                  label: const Text("Add speaker"))
                            ]);
                          } else if (entry.key == guestsKey) {
                            return Row(children: [
                              Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Icon(Icons.person_rounded,
                                      color: styles.themeColors.text)),
                              Text(entry.key,
                                  style: Theme.of(context).textTheme.headline5),
                              const Spacer(),
                              ElevatedButton.icon(
                                  onPressed: () {
                                    /// add guest
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const AddGuest(
                                                    key: Key("AddGuest"))));
                                  },
                                  icon: const Icon(Icons.add_rounded),
                                  label: const Text("Add Guest"))
                            ]);
                          } else if (entry.key == promoKey) {
                            return Row(children: [
                              Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Icon(Icons.business_rounded,
                                      color: styles.themeColors.text)),
                              Text(entry.key,
                                  style: Theme.of(context).textTheme.headline5),
                              const Spacer(),
                              ElevatedButton.icon(
                                  onPressed: () {
                                    /// add promo

                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const AddPromo(
                                                    key: Key("AddPromo"))));
                                  },
                                  icon: const Icon(Icons.add_rounded),
                                  label: const Text("Add Promo"))
                            ]);
                          }
                          return Row(children: [
                            Padding(
                                padding:
                                    const EdgeInsets.all(8.0).copyWith(left: 0),
                                child: Icon(Icons.verified_user_rounded,
                                    color: styles.themeColors.text)),
                            Text(entry.key,
                                style: Theme.of(context).textTheme.headline5),
                            const Spacer(),
                            ElevatedButton.icon(
                                onPressed: () {
                                  /// TODO add admin
                                },
                                icon: const Icon(Icons.add_rounded),
                                label: const Text("Add admin"))
                          ]);
                        }))),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                  final user = entry.value[index];

                  if (user.hasSpeaker()) {
                    return TileRow(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AddSpeaker(
                                  id: user.id, key: const Key("AddSpeaker"))));
                        },
                        children: [
                          Text(
                              "${user.speaker.firstname} ${user.speaker.lastname}"),
                          Text(user.speaker.city),
                          Text(user.speaker.company),
                          Text(user.speaker.job)
                        ]);
                  }
                  if (user.hasPromo()) {
                    return TileRow(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AddPromo(
                                  id: user.id, key: const Key("AddPromo"))));
                        },
                        children: [
                          Text(user.promo.title),
                          Text(user.promo.address),
                          Text(user.promo.about),
                        ]);
                  }
                  if (user.hasGuest()) {
                    return TileRow(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AddGuest(
                                  id: user.id, key: const Key("AddGuest"))));
                        },
                        children: [Text(user.guest.nickname)]);
                  }
                  return TileRow(onPressed: null, children: [
                    Text(user.login),
                    Text(user.role),
                  ]);
                }, childCount: entry.value.length))
              ])
            ]
          ]);
        },
        error: (e, s) => Text(e.toString()),
        loading: () => styles.loadingAnimation);
  }
}

class TileRow extends StatelessWidget {
  final List<Widget> children;
  final VoidCallback? onPressed;

  const TileRow({Key? key, required this.children, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Material(
        borderRadius: BorderRadius.circular(10),
        clipBehavior: Clip.antiAlias,
        color: Colors.transparent,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: styles.themeColors.cardBackground)),
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Row(children: [
              // if (children.isNotEmpty) ...[Padding(
              //   padding: const EdgeInsets.only(right: 10.0),
              //   child: children[0],
              // )],
              if (children.isNotEmpty) ...[
                Expanded(flex: 2, child: children[0])
              ],
              if (children.length > 1) ...[
                Expanded(flex: 1, child: children[1])
              ],
              if (children.length > 2) ...[
                Expanded(flex: 1, child: children[2])
              ],
              if (children.length > 3) ...[
                Expanded(flex: 1, child: children[3])
              ],
              IconButton(
                  onPressed: onPressed,
                  icon: const Icon(Icons.more_horiz_rounded))
            ]),
          ),
        ),
      ),
    );
  }
}

class FindSpeaker extends ConsumerStatefulWidget {
  const FindSpeaker({Key? key}) : super(key: key);

  @override
  ConsumerState<FindSpeaker> createState() => _FindSpeakerState();
}

class _FindSpeakerState extends ConsumerState<FindSpeaker> {
  @override
  Widget build(BuildContext context) {
    final listProvider =
        ref.watch(findSpeakerProvider(ref.watch(selectedConference).id));

    return CustomScrollView(slivers: [
      const SliverToBoxAdapter(
          child: Padding(
              padding: EdgeInsets.only(bottom: 12.0),
              child: SearchField(SearchType.speaker))),
      listProvider.when(
          data: (list) {
            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              final user = list?[index];

              if (user == null) {
                return const SizedBox.shrink();
              }

              if (user.hasSpeaker()) {
                return TileRow(
                    onPressed: () {
                      Navigator.of(context).pop(user);
                    },
                    children: [
                      Text(
                          "${user.speaker.firstname} ${user.speaker.lastname}"),
                      Text(user.speaker.city),
                      Text(user.speaker.company),
                      Text(user.speaker.job)
                    ]);
              }
              if (user.hasPromo()) {
                return TileRow(
                    onPressed: () {
                      Navigator.of(context).pop(user);
                    },
                    children: [
                      Text(user.promo.title),
                      Text(user.promo.address),
                      Text(user.promo.about),
                    ]);
              }
            }, childCount: list?.length ?? 0));
          },
          error: (e, s) => SliverToBoxAdapter(child: Text(e.toString())),
          loading: () => const SliverToBoxAdapter())
    ]);
  }
}
