import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/guests/common.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

class AddPromo extends ConsumerStatefulWidget {
  final String? id;

  const AddPromo({Key? key, this.id}) : super(key: key);

  @override
  _AddPromoState createState() => _AddPromoState();
}

class _AddPromoState extends ConsumerState<AddPromo> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _nickNameController = TextEditingController();
  final TextEditingController _aboutController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _telegramController = TextEditingController();
  final TextEditingController _vkController = TextEditingController();

  final ValueNotifier<Image?> _imageController = ValueNotifier<Image?>(null);

  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final serverPromo =
        ref.watch(oneUserProvider(SingleItem(conf.id, widget.id ?? "")));

    Promo localPromo = Promo();

    if (localPromo.id.isEmpty && (serverPromo.value != null)) {
      localPromo = serverPromo.value!.promo;
    }

    submit() {
      if (_formKey.currentState?.validate() ?? false) {
        _formKey.currentState?.save();
        log(localPromo.toDebugString());
        Navigator.of(context).pop();
      }
    }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: styles.themeColors.background.withOpacity(0.8),
          elevation: 0,
          title: Text("Add new promo",
              style: Theme.of(context).textTheme.headline6),
          actions: [
            TextButton(
                onPressed: () {
                  /// добавить спикера
                  submit();
                },
                child: const Text("Add promo")),
            const SizedBox(width: 25),
            TextButton(
                style: TextButton.styleFrom(primary: styles.themeColors.accent),
                onPressed: () {
                  submit();

                  /// TODO отправить приглашение
                },
                child: const Text("Send invite")),
          ]),
      extendBodyBehindAppBar: true,
      extendBody: true,
      bottomNavigationBar: Container(
        color: styles.themeColors.background.withOpacity(0.8),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background)),
              onPressed: () {
                /// добавить спикера
                submit();
              },
              child: const Text("Add promo")),
          const SizedBox(width: 15),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background),
                  primary: styles.themeColors.accent),
              onPressed: () {
                submit();

                /// TODO отправить приглашение
              },
              child: const Text("Send invite")),
        ]),
      ),
      body: Row(children: [
        Expanded(
          flex: 3,
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(padding: const EdgeInsets.only(top: 60), children: [
              /// Title
              TextFormField(
                  controller: _titleController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Title", hintText: "Enter company title..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "Enter title";
                    }
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localPromo.title = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// nickname
              TextFormField(
                  controller: _nickNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(30)],
                  decoration: const InputDecoration(
                      labelText: "Nickname", hintText: "Enter nickname..."),
                  onSaved: (value) {
                    if (value != null) {
                      localPromo.nickname = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// about
              TextFormField(
                  minLines: 5,
                  maxLines: 5,
                  controller: _aboutController,
                  maxLength: 500,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  decoration: const InputDecoration(
                      labelText: "About promo",
                      hintText: "Enter some otional information..."),
                  validator: (value) {
                    if ((value?.isNotEmpty ?? false) &&
                        ((value?.length ?? 0) < 100)) {
                      return "Please, enter more information";
                    }
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localPromo.about = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// address
              TextFormField(
                  controller: _addressController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Address", hintText: "Address to display"),
                  onSaved: (value) {
                    if (value != null) {
                      localPromo.address = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// email
              Row(children: [
                Expanded(
                  child: TextFormField(
                      controller: _emailController,
                      inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      decoration: const InputDecoration(
                          labelText: "Email", hintText: "email@example.com"),
                      validator: (value) {
                        if (value?.isEmpty ?? true) {
                          return "It's necessary to enter email";
                        }
                      },
                      onSaved: (value) {
                        if (value != null) {
                          localPromo.email = value;
                        }
                      }),
                ),
                const SizedBox(width: 15),

                /// phone
                Expanded(
                  child: TextFormField(
                      controller: _phoneController,
                      inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      decoration: const InputDecoration(
                          labelText: "Phone", hintText: "+79990000000"),
                      onSaved: (value) {
                        if (value != null) {
                          localPromo.phone = value;
                        }
                      }),
                ),
              ]),
              const SizedBox(height: 15),

              Row(
                children: [
                  /// telegram
                  Expanded(
                    child: TextFormField(
                        controller: _telegramController,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        decoration: const InputDecoration(
                            labelText: "Telegram", hintText: "@example"),
                        onSaved: (value) {
                          if (value != null) {
                            localPromo.telegram = value;
                          }
                        }),
                  ),
                  const SizedBox(width: 15),

                  /// vk
                  Expanded(
                    child: TextFormField(
                        controller: _vkController,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        decoration: const InputDecoration(
                            labelText: "Vk", hintText: "@example"),
                        onSaved: (value) {
                          if (value != null) {
                            localPromo.vk = value;
                          }
                        }),
                  ),
                ],
              ),

              const SizedBox(height: 60),
            ]),
          ),
        ),
        const SizedBox(width: 25),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.2,
          child: SafeArea(
            child: Column(children: [
              Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: _imageController.value?.image ??
                              MemoryImage(Uint8List.fromList([]))),
                      shape: BoxShape.circle,
                      color: styles.themeColors.brand),
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: MediaQuery.of(context).size.width * 0.2,
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          try {
                            _imageController.value =
                                await ImagePickerWeb.getImageAsWidget();
                          } catch (e) {
                            log(e.toString());
                          }
                          setState(() {});
                        },
                        child: Column(children: [
                          const Spacer(),
                          Container(
                              width: double.infinity,
                              padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10)
                                  .copyWith(
                                      bottom:
                                          MediaQuery.of(context).size.width *
                                              0.01),
                              color: styles.themeColors.text,
                              child: Center(
                                child: Text("Change photo",
                                    style: TextStyle(
                                        color:
                                            styles.themeColors.invertedText)),
                              ))
                        ]),
                      ))),
              const SizedBox(height: 25),
              StatusDropDown(
                  value: UserStatus.values.firstWhere(
                      (element) => element.toString() == localPromo.status,
                      orElse: () => UserStatus.created),
                  onSaved: (status) => localPromo.status = status.toString())
            ]),
          ),
        )
      ]),
    );
  }
}
