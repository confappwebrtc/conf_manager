import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';
import 'common.dart';

class AddSpeaker extends ConsumerStatefulWidget {
  final String? id;

  const AddSpeaker({Key? key, this.id}) : super(key: key);

  @override
  _AddSpeakerState createState() => _AddSpeakerState();
}

class _AddSpeakerState extends ConsumerState<AddSpeaker> {
  final _formKey = GlobalKey<FormState>();

  FormStatus status = FormStatus.view;

  @override
  void initState() {
    if (widget.id == null) {
      status = FormStatus.create;
    }
    super.initState();
  }

  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController middleNameController = TextEditingController();
  final TextEditingController nickNameController = TextEditingController();
  final TextEditingController aboutController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController jobController = TextEditingController();
  final TextEditingController companyController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController telegramController = TextEditingController();
  final TextEditingController vkController = TextEditingController();

  final ValueNotifier<Sex> sexController = ValueNotifier<Sex>(Sex.other);
  final ValueNotifier<Image?> imageController = ValueNotifier<Image?>(null);

  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final serverSpeaker =
        ref.watch(oneUserProvider(SingleItem(conf.id, widget.id ?? "")));

    Speaker localSpeaker = Speaker();

    if (localSpeaker.id.isEmpty && (serverSpeaker.value != null)) {
      localSpeaker = serverSpeaker.value!.speaker;
      Sex gender = Sex.other;

      switch (localSpeaker.gender) {
        case "female":
          {
            gender = Sex.female;
            break;
          }
        case "male":
          {
            gender = Sex.female;
            break;
          }
        default:
          {
            gender = Sex.other;
            break;
          }
      }

      lastNameController.text = localSpeaker.lastname;
      firstNameController.text = localSpeaker.firstname;
      middleNameController.text = localSpeaker.middlename;
      nickNameController.text = localSpeaker.nickname;
      aboutController.text = localSpeaker.about;
      cityController.text = localSpeaker.city;
      jobController.text = localSpeaker.job;
      companyController.text = localSpeaker.company;
      emailController.text = localSpeaker.email;
      phoneController.text = localSpeaker.phone;
      telegramController.text = localSpeaker.telegram;
      vkController.text = localSpeaker.vk;
      sexController.value = gender;
    }

    submit() {
      if (_formKey.currentState?.validate() ?? false) {
        _formKey.currentState?.save();
        log(localSpeaker.toDebugString());
        Navigator.of(context).pop();
      }
    }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: styles.themeColors.background.withOpacity(0.8),
          elevation: 0,
          title: Text(
              status == FormStatus.create ? "Add new speaker" : "Speaker",
              style: Theme.of(context).textTheme.headline6),
          actions: [
            if (status != FormStatus.view)
              TextButton(
                  onPressed: () {
                    /// добавить спикера
                    submit();
                  },
                  child: Text(
                      status == FormStatus.create ? "Add new speaker" : "Save"))
            else
              TextButton(
                  onPressed: () {
                    setState(() {
                      status = FormStatus.edit;
                    });
                  },
                  child: const Text("Edit")),
            const SizedBox(width: 25),
            TextButton(
                style: TextButton.styleFrom(primary: styles.themeColors.accent),
                onPressed: () {
                  submit();

                  /// TODO отправить приглашение
                },
                child: const Text("Send invite")),
          ]),
      extendBodyBehindAppBar: true,
      extendBody: true,
      bottomNavigationBar: Container(
        color: styles.themeColors.background.withOpacity(0.8),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          if (status != FormStatus.view)
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    textStyle: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: styles.themeColors.background)),
                onPressed: () {
                  /// добавить спикера
                  submit();
                },
                child: Text(
                    status == FormStatus.create ? "Add new speaker" : "Save"))
          else
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    textStyle: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: styles.themeColors.danger)),
                onPressed: () {
                  setState(() {
                    status = FormStatus.edit;
                  });
                },
                child: const Text("Edit")),
          const SizedBox(width: 15),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background),
                  primary: styles.themeColors.accent),
              onPressed: () {
                submit();

                /// TODO отправить приглашение
              },
              child: const Text("Send invite")),
        ]),
      ),
      body: Row(children: [
        Expanded(
          flex: 3,
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(padding: const EdgeInsets.only(top: 60), children: [
              /// Lastname
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  controller: lastNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Lastname", hintText: "Enter lastname..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "Enter lastname";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.lastname = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// firstname
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  controller: firstNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Firstname", hintText: "Enter firstname..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "Enter firstname";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.firstname = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// middleName
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  controller: middleNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "Middle name",
                      hintText: "Enter middle name..."),
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.middlename = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// nickname
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  controller: nickNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(30)],
                  decoration: const InputDecoration(
                      labelText: "Nickname", hintText: "Enter nickname..."),
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.nickname = value;
                    }
                  }),
              const SizedBox(height: 15),

              Row(children: [
                Text("Gender",
                    style: TextStyle(color: styles.themeColors.brandedText)),
                const SizedBox(width: 15),
                CupertinoSegmentedControl<Sex>(
                    groupValue: sexController.value,
                    onValueChanged: (value) {
                      if (status != FormStatus.view) {
                        setState(() {
                          sexController.value = value;
                        });
                        localSpeaker.gender = value.toString();
                      }
                    },
                    children: const {
                      Sex.male: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                          child: Text("Male")),
                      Sex.female: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                          child: Text("Female")),
                      Sex.other: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                          child: Text("Other")),
                    })
              ]),
              const SizedBox(height: 15),

              /// about
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  minLines: 5,
                  maxLines: 5,
                  controller: aboutController,
                  maxLength: 500,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  decoration: const InputDecoration(
                      labelText: "About speaker",
                      hintText: "Enter some otional information..."),
                  validator: (value) {
                    if ((value?.isNotEmpty ?? false) &&
                        ((value?.length ?? 0) < 100)) {
                      return "Please, enter more information";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.about = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// city
              TextFormField(
                  enabled: !(status == FormStatus.view),
                  controller: cityController,
                  inputFormatters: [LengthLimitingTextInputFormatter(100)],
                  decoration: const InputDecoration(
                      labelText: "City", hintText: "City to display"),
                  onSaved: (value) {
                    if (value != null) {
                      localSpeaker.city = value;
                    }
                  }),
              const SizedBox(height: 15),

              Row(
                children: [
                  /// company
                  Expanded(
                    child: TextFormField(
                        enabled: !(status == FormStatus.view),
                        controller: companyController,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(100)
                        ],
                        decoration: const InputDecoration(
                            labelText: "Company",
                            hintText: "Enter company of job..."),
                        onSaved: (value) {
                          if (value != null) {
                            localSpeaker.company = value;
                          }
                        }),
                  ),
                  const SizedBox(width: 15),

                  /// job
                  Expanded(
                    child: TextFormField(
                        enabled: !(status == FormStatus.view),
                        controller: jobController,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(100)
                        ],
                        decoration: const InputDecoration(
                            labelText: "Job", hintText: "Enter primary job..."),
                        onSaved: (value) {
                          if (value != null) {
                            localSpeaker.job = value;
                          }
                        }),
                  ),
                ],
              ),
              const SizedBox(height: 15),

              /// email
              Row(children: [
                Expanded(
                  child: TextFormField(
                      enabled: !(status == FormStatus.view),
                      controller: emailController,
                      inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      decoration: const InputDecoration(
                          labelText: "Email", hintText: "email@example.com"),
                      validator: (value) {
                        if (value?.isEmpty ?? true) {
                          return "It's necessary to enter email";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        if (value != null) {
                          localSpeaker.email = value;
                        }
                      }),
                ),
                const SizedBox(width: 15),

                /// phone
                Expanded(
                  child: TextFormField(
                      enabled: !(status == FormStatus.view),
                      controller: phoneController,
                      inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      decoration: const InputDecoration(
                          labelText: "Phone", hintText: "+79990000000"),
                      onSaved: (value) {
                        if (value != null) {
                          localSpeaker.phone = value;
                        }
                      }),
                ),
              ]),
              const SizedBox(height: 15),

              Row(
                children: [
                  /// telegram
                  Expanded(
                    child: TextFormField(
                        enabled: !(status == FormStatus.view),
                        controller: telegramController,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        decoration: const InputDecoration(
                            labelText: "Telegram", hintText: "@example"),
                        onSaved: (value) {
                          if (value != null) {
                            localSpeaker.telegram = value;
                          }
                        }),
                  ),
                  const SizedBox(width: 15),

                  /// vk
                  Expanded(
                    child: TextFormField(
                        enabled: !(status == FormStatus.view),
                        controller: vkController,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        decoration: const InputDecoration(
                            labelText: "Vk", hintText: "@example"),
                        onSaved: (value) {
                          if (value != null) {
                            localSpeaker.vk = value;
                          }
                        }),
                  ),
                ],
              ),

              const SizedBox(height: 60),
            ]),
          ),
        ),
        const SizedBox(width: 25),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.2,
          child: SafeArea(
            child: Column(children: [
              Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: imageController.value?.image ??
                              MemoryImage(Uint8List.fromList([]))),
                      shape: BoxShape.circle,
                      color: styles.themeColors.brand),
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: MediaQuery.of(context).size.width * 0.2,
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: status == FormStatus.view
                            ? null
                            : () async {
                                try {
                                  imageController.value =
                                      await ImagePickerWeb.getImageAsWidget();
                                } catch (e) {
                                  log(e.toString());
                                }
                                setState(() {});
                              },
                        child: Column(children: [
                          const Spacer(),
                          if (status != FormStatus.view)
                            Container(
                                width: double.infinity,
                                padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10)
                                    .copyWith(
                                        bottom:
                                            MediaQuery.of(context).size.width *
                                                0.01),
                                color: styles.themeColors.text,
                                child: Center(
                                  child: Text("Change photo",
                                      style: TextStyle(
                                          color:
                                              styles.themeColors.invertedText)),
                                ))
                        ]),
                      ))),
              const SizedBox(height: 25),
              StatusDropDown(
                  value: UserStatus.values.firstWhere(
                      (element) => element.toString() == localSpeaker.status,
                      orElse: () => UserStatus.created),
                  onSaved: (status) => localSpeaker.status = status.toString())
            ]),
          ),
        )
      ]),
    );
  }
}
