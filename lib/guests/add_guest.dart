import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/providers/common.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

class AddGuest extends ConsumerStatefulWidget {
  final String? id;

  const AddGuest({Key? key, this.id}) : super(key: key);

  @override
  _AddGuestState createState() => _AddGuestState();
}

class _AddGuestState extends ConsumerState<AddGuest> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _nickNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  final ValueNotifier<Image?> _imageController = ValueNotifier<Image?>(null);

  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final serverGuest =
        ref.watch(oneUserProvider(SingleItem(conf.id, widget.id ?? "")));

    User localUser = User();
    Guest localGuest = Guest();

    if (localGuest.id.isEmpty && (serverGuest.value != null)) {
      localGuest = serverGuest.value!.guest;
    }

    submit() {
      if (_formKey.currentState?.validate() ?? false) {
        _formKey.currentState?.save();
        log(localGuest.toDebugString());
        Navigator.of(context).pop();
      }
    }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: styles.themeColors.background.withOpacity(0.8),
          elevation: 0,
          title: Text("Add new guest",
              style: Theme.of(context).textTheme.headline6),
          actions: [
            TextButton(
                onPressed: () {
                  /// добавить спикера
                  submit();
                },
                child: const Text("Add guest")),
            const SizedBox(width: 25),
            TextButton(
                style: TextButton.styleFrom(primary: styles.themeColors.accent),
                onPressed: () {
                  submit();

                  /// TODO отправить приглашение
                },
                child: const Text("Send invite")),
          ]),
      extendBodyBehindAppBar: true,
      extendBody: true,
      bottomNavigationBar: Container(
        color: styles.themeColors.background.withOpacity(0.8),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background)),
              onPressed: () {
                /// добавить спикера
                submit();
              },
              child: const Text("Add guest")),
          const SizedBox(width: 15),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: styles.themeColors.background),
                  primary: styles.themeColors.accent),
              onPressed: () {
                submit();

                /// TODO отправить приглашение
              },
              child: const Text("Send invite")),
        ]),
      ),
      body: Row(children: [
        Expanded(
          flex: 3,
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(padding: const EdgeInsets.only(top: 60), children: [
              /// login
              TextFormField(
                  controller: _loginController,
                  inputFormatters: [LengthLimitingTextInputFormatter(30)],
                  decoration: const InputDecoration(
                      labelText: "Login", hintText: "Enter login..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "It's necessary to enter login";
                    }
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localUser.login = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// nickname
              TextFormField(
                  controller: _nickNameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(30)],
                  decoration: const InputDecoration(
                      labelText: "Nickname", hintText: "Enter nickname..."),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "It's necessary to enter nickname";
                    }
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localGuest.nickname = value;
                    }
                  }),
              const SizedBox(height: 15),

              /// email
              /* TextFormField(
                  controller: emailController,
                  inputFormatters: [LengthLimitingTextInputFormatter(30)],
                  decoration: const InputDecoration(
                      labelText: "Email", hintText: "email@example.com"),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return "It's necessary to enter email";
                    }
                  },
                  onSaved: (value) {
                    if (value != null) {
                      localGuest. = value;
                    }
                  }), */

              const SizedBox(height: 60),
            ]),
          ),
        ),
        const SizedBox(width: 25),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.2,
          child: SafeArea(
            child: Column(children: [
              Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: _imageController.value?.image ??
                              MemoryImage(Uint8List.fromList([]))),
                      shape: BoxShape.circle,
                      color: styles.themeColors.brand),
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: MediaQuery.of(context).size.width * 0.2,
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          try {
                            _imageController.value =
                                await ImagePickerWeb.getImageAsWidget();
                          } catch (e) {
                            log(e.toString());
                          }
                          setState(() {});
                        },
                        child: Column(children: [
                          const Spacer(),
                          Container(
                              width: double.infinity,
                              padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10)
                                  .copyWith(
                                      bottom:
                                          MediaQuery.of(context).size.width *
                                              0.01),
                              color: styles.themeColors.text,
                              child: Center(
                                child: Text("Change photo",
                                    style: TextStyle(
                                        color:
                                            styles.themeColors.invertedText)),
                              ))
                        ]),
                      ))),
              const SizedBox(height: 25),
              OutlinedButton.icon(
                  icon: const Icon(Icons.block_rounded),
                  onPressed: () {
                    /// TODO block user
                  },
                  label: const Text("Block guest"))
            ]),
          ),
        )
      ]),
    );
  }
}
