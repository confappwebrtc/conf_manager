import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/generated/proto/user.pb.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

class CurrentUserButton extends ConsumerWidget {
  const CurrentUserButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final user = ref.watch(userProvider.state);
    if (user.state.id.isEmpty) {
      return const SizedBox.shrink();
    }
    return Material(
      clipBehavior: Clip.antiAlias,
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(21),
      child: PopupMenuButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: styles.themeColors.background.withOpacity(0.9),
          offset: const Offset(0, 50),
          child: Chip(
              backgroundColor: styles.themeColors.brandDarker,
              padding: EdgeInsets.zero,
              labelPadding:
                  const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
              side: BorderSide(color: styles.themeColors.brand),
              avatar: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: styles.themeColors.brand)),
              label: Row(
                children: [
                  Text(user.state.login,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: styles.themeColors.onBrandText)),
                  const SizedBox(width: 10),
                  Icon(Icons.arrow_drop_down_rounded,
                      size: 30, color: styles.themeColors.onBrandText),
                ],
              )),
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                  onTap: () {
                    /// TODO страница редактирования профиля
                  },
                  child: Row(children: [
                    Icon(Icons.manage_accounts_rounded,
                        color: styles.themeColors.brand),
                    const SizedBox(width: 10),
                    const Text("Edit Profile"),
                  ])),
              PopupMenuItem(
                  onTap: () {
                    user.state = User.getDefault();
                  },
                  child: Row(children: [
                    Icon(Icons.logout_rounded, color: styles.themeColors.brand),
                    const SizedBox(width: 10),
                    const Text("Log out"),
                  ])),
            ];
          }),
    );
  }
}
