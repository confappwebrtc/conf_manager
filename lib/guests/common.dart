import 'package:flutter/material.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

enum FormStatus { create, view, edit }

class StatusDropDown extends StatelessWidget {
  final UserStatus value;
  final Function(UserStatus) onSaved;
  const StatusDropDown({Key? key, required this.value, required this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<UserStatus>(
        style: Theme.of(context).textTheme.button,
        dropdownColor: styles.themeColors.background,
        value: value,
        onSaved: (status) {
          if (status == null) {
            onSaved(status!);
          }
        },
        onChanged: (status) {
          if (status == null) {
            onSaved(status!);
          }
        },
        items: [
          DropdownMenuItem(
              child: Text("Created",
                  style: TextStyle(color: styles.themeColors.brand)),
              value: UserStatus.created),
          DropdownMenuItem(
              child: Text("Editing",
                  style: TextStyle(color: styles.themeColors.brandDarker)),
              value: UserStatus.onEdit),
          DropdownMenuItem(
              child: Text("Active",
                  style: TextStyle(color: styles.themeColors.accent)),
              value: UserStatus.active),
          DropdownMenuItem(
              child: Text("Denied",
                  style: TextStyle(color: styles.themeColors.danger)),
              value: UserStatus.denied),
        ]);
  }
}
