import 'dart:async';

import 'package:flutter/material.dart';
import 'package:manager/styles.dart';

import 'exception_dialog.dart';

typedef WaiterFunction<T> = FutureOr<T> Function();

FutureOr<T?> blockUiOnAsyncCode<T>(
    BuildContext childContext, WaiterFunction<T> block) async {
  final scaffoldContext = Navigator.of(childContext).context;
  showDialog(
      context: scaffoldContext,
      barrierDismissible: false,
      barrierColor: styles.themeColors.background.withOpacity(0.75),
      useRootNavigator: false,
      builder: (context) => WillPopScope(
          onWillPop: () async => false,
          child: Center(
              child: SizedBox(
                  width: 150, height: 150, child: styles.loadingAnimation))));
  try {
    T res = await block();
    return res;
  } catch (error) {
    await showDialog(
        context: scaffoldContext,
        builder: (context) => ExceptionDialog((error is ExceptionCause)
            ? error
            : ExceptionCause(error, "Something went wrong")));
  } finally {
    Navigator.of(scaffoldContext).pop();
  }
  return null;
}
