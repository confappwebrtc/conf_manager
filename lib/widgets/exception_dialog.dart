import 'package:flutter/material.dart';
import 'package:manager/styles.dart';
import 'package:manager/widgets/simple_spoiler.dart';

class ExceptionCause implements Exception {
  final dynamic cause;
  final String message;

  ExceptionCause(this.cause, [this.message = ""]);
}

class ExceptionDialog extends StatelessWidget {
  final Object e;
  final Color? background;

  const ExceptionDialog(this.e, {Key? key, this.background}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool withUnlockPayment = false;
    var defaultErrorText = "";
    if (e is ExceptionCause) {
      defaultErrorText = (e as ExceptionCause).message;
    }

    return AlertDialog(
        scrollable: true,
        titlePadding: EdgeInsets.zero,
        contentPadding: const EdgeInsets.all(24).copyWith(top: 0),
        buttonPadding: EdgeInsets.zero,
        backgroundColor: background ?? styles.themeColors.cardBackground,
        title: Icon(Icons.error_rounded,
            color: styles.themeColors.danger, size: 96),
        content: Column(mainAxisSize: MainAxisSize.min, children: [
          Text(
              ((e is ExceptionCause) &&
                      (e as ExceptionCause).message.isNotEmpty)
                  ? (e as ExceptionCause).message.toString()
                  : "Something went wrong.\nTry again later",
              style: const TextStyle(height: 1.2)),
          Container(height: 15),
          SimpleSpoiler(
            innerChild: Text(
                (e is ExceptionCause)
                    ? (e as ExceptionCause).cause.toString()
                    : e.toString(),
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: styles.themeColors.caption)),
            labelText: Text("Developer details",
                style: Theme.of(context)
                    .textTheme
                    .button!
                    .copyWith(color: styles.themeColors.brandedText)),
          )
        ]),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text("Close"))
        ]);
  }
}
