import 'dart:async';
import 'package:flutter/material.dart';
import 'package:manager/styles.dart';

Future<T?> showSuccessDialog<T>(BuildContext context,
    {String? message,
    Duration delay = const Duration(milliseconds: 1000),
    Widget? child}) async {
  final nav = Navigator.of(context);
  final timer = Timer(delay, () {
    nav.pop();
  });
  final T? res = await showDialog<T?>(
      context: nav.context,
      useRootNavigator: false,
      barrierColor: styles.themeColors.background.withOpacity(0.75),
      builder: (context) {
        return Center(
          child: Card(
            elevation: 0,
            color: styles.themeColors.background,
            margin: const EdgeInsets.all(25).copyWith(top: 0),
            child: Container(
              padding: (message ?? "").isNotEmpty
                  ? const EdgeInsets.all(15).copyWith(top: 0)
                  : EdgeInsets.zero,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.check_circle_rounded,
                      color: styles.themeColors.accent,
                      size: 96,
                    ),
                    if ((message != null) && (message.isNotEmpty))
                      Text(message,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.subtitle1),
                    if (child != null) child
                  ]),
            ),
          ),
        );
      });
  timer.cancel();
  return res;
}
