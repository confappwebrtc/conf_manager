import 'package:flutter/material.dart';
import 'package:manager/styles.dart';

class SimpleSpoiler extends StatefulWidget {
  final Widget innerChild;

  /// если указать label, то вместо кнопки будет использован он
  final Widget? label;

  /// если нет label, то подставляется labelText в шаблон строки инфо
  final Widget? labelText;

  /// виджет кнопки, по нажатию на которую будет скрываться спойлер
  final Widget? hide;

  final TextStyle? labelStyle;
  final TextStyle? innerStyle;

  const SimpleSpoiler(
      {Key? key,
        required this.innerChild,
        this.label,
        this.labelText,
        this.hide,
        this.labelStyle,
        this.innerStyle})
      : super(key: key);

  @override
  State createState() => _SimpleSpoilerState();
}

class _SimpleSpoilerState extends State<SimpleSpoiler>
    with SingleTickerProviderStateMixin {
  final ValueNotifier<bool> _showMore = ValueNotifier<bool>(false);
  late AnimationController _controller;
  late Animation<double> _scaleAnimation;

  @override
  void initState() {
    super.initState();
    _showMore.addListener(activateShowMore);
    _controller = AnimationController(
      duration: Common.animationDuration,
      vsync: this,
    );
    _scaleAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInQuart,
    ));
    _controller.addListener(_updateScroll);
  }

  void _updateScroll() {
    if (_showMore.value) {
      Scrollable.ensureVisible(context);
    }
    /*widget.scrollController
        ?.jumpTo(widget.scrollController?.position.maxScrollExtent ?? 0);*/
  }

  @override
  void dispose() {
    _controller.removeListener(_updateScroll);
    _controller.dispose();
    _showMore.removeListener(activateShowMore);
    super.dispose();
  }

  void activateShowMore() async {
    if (mounted) setState(() {});
    if (_showMore.value) {
      await _controller.animateTo(1);
    } else {
      await _controller.animateBack(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextButton(
          onPressed: () {
            _showMore.value = !_showMore.value;
          },
          style: TextButton.styleFrom(splashFactory: NoSplash.splashFactory),
          child: DefaultTextStyle(
              style: widget.labelStyle ??
                  Theme.of(context)
                      .textTheme
                      .overline!
                      .copyWith(color: styles.themeColors.brandedText),
              child: widget.label ??
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(Icons.help_outline_rounded,
                            color: styles.themeColors.brandedText, size: 18),
                        Container(width: 10),
                        Expanded(child: widget.labelText ?? const Text("")),
                      ])),
        ),
        SizeTransition(
            sizeFactor: _scaleAnimation,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  DefaultTextStyle(
                      style: widget.innerStyle ??
                          Theme.of(context).textTheme.caption!,
                      textAlign: TextAlign.justify,
                      child: widget.innerChild),
                  TextButton(
                    onPressed: () {
                      _showMore.value = false;
                    },
                    style: TextButton.styleFrom(
                        splashFactory: NoSplash.splashFactory),
                    child: DefaultTextStyle(
                        style: widget.labelStyle ??
                            Theme.of(context)
                                .textTheme
                                .overline!
                                .copyWith(color: styles.themeColors.brandedText),
                        textAlign: TextAlign.justify,
                        child: widget.hide ?? const Text("Hide")),
                  )
                ])),
      ],
    );
  }
}
