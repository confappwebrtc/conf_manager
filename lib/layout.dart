import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/conf/conf_list.dart';
import 'package:manager/guests/current_user.dart';
import 'package:manager/guests/guests.dart';
import 'package:manager/login.dart';
import 'package:manager/providers/api.dart';
import 'package:manager/providers/conf_provider.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

import 'conf/content.dart';
import 'conf/schedule.dart';

final currentPageIndexProvider = StateProvider((ref) => 0);
// final currentPageProvider = StateProvider<Widget?>((ref) => null);

class MainLayout extends ConsumerStatefulWidget {
  const MainLayout({Key? key}) : super(key: key);

  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends ConsumerState<MainLayout> {
  @override
  Widget build(BuildContext context) {
    final currentUser = ref.watch(userProvider.state);

    final _selectedIndex = ref.watch(currentPageIndexProvider.state);
    final _conference = ref.watch(selectedConference);

    if (currentUser.state.id.isEmpty) {
      return const LoginView();
    }

    return Scaffold(
      appBar: AppBar(
        foregroundColor: styles.themeColors.text,
        flexibleSpace: Container(
            decoration:
                BoxDecoration(gradient: styles.panelDecoration.gradient)),
        title: DefaultTextStyle(
            style: Theme.of(context)
                .textTheme
                .headline5!
                .copyWith(color: styles.themeColors.onBrandText),
            child: Row(children: [
              const Text("Conference"),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(Icons.arrow_forward_ios_rounded,
                    size: 20, color: styles.themeColors.onBrandText),
              ),
              Text(_conference.title),
            ])),
        actions: [
          const CurrentUserButton(),
          TextButton(
              onPressed: () async {
                if (await checkConnection()) {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(
                            content: Text("Connection is active",
                                style: TextStyle(color: Colors.green)));
                      });
                } else {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(
                            content: Text("Something wrong",
                                style: TextStyle(color: Colors.red)));
                      });
                }
              },
              child: const Text("Check connection"))
        ],
      ),
      body: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          margin: const EdgeInsets.only(top: 10, left: 15, right: 20),
          padding: const EdgeInsets.all(5),
          height: 320,
          decoration: BoxDecoration(
              gradient: styles.plateDecoration.gradient,
              borderRadius: BorderRadius.circular(21)),
          child: NavigationRail(
              trailing: const SizedBox(height: 20),
              backgroundColor: Colors.transparent,
              selectedIndex: _selectedIndex.state,
              onDestinationSelected: (int index) {
                _selectedIndex.state = index;
              },
              groupAlignment: 0,
              unselectedIconTheme: IconThemeData(
                  color: styles.themeColors.onBrandText, size: 42, opacity: 1),
              selectedIconTheme: IconThemeData(
                  color: styles.themeColors.brand, size: 42, opacity: 1),
              unselectedLabelTextStyle: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: styles.themeColors.onBrandText),
              selectedLabelTextStyle: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: styles.themeColors.brandedText),
              labelType: NavigationRailLabelType.all,
              destinations: const <NavigationRailDestination>[
                NavigationRailDestination(
                  icon: Icon(Icons.cottage_rounded),
                  label: Text('Home'),
                ),
                NavigationRailDestination(
                  icon: Icon(Icons.event_note_rounded),
                  label: Text('Schedule'),
                ),
                NavigationRailDestination(
                  icon: Icon(Icons.badge_rounded),
                  label: Text('Guests'),
                ),
                /*NavigationRailDestination(
                      icon: Icon(Icons.content_paste_rounded),
                      label: Text('Content'),
                    ),*/
              ]),
        ),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: MaterialApp(
                  theme: Theme.of(context),
                  debugShowCheckedModeBanner: false,
                  home: AnimatedSwitcher(
                      duration: Common.animationDuration,
                      child: <Widget>[
                        const ConfList(key: Key("confList")),
                        const Schedule(key: Key("schedule")),
                        const Guests(key: Key("guests")),
                        const Content(key: Key("content")),
                      ][_selectedIndex.state]))),
        )
      ]),
    );
  }
}
