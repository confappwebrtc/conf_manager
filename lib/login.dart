import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:manager/providers/users_provider.dart';
import 'package:manager/styles.dart';

import 'generated/proto/user.pb.dart';

class LoginView extends ConsumerStatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends ConsumerState<LoginView> {
  bool visib = false;
  final _emKey = GlobalKey<FormState>();
  final _pasKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final currentUser = ref.watch(userProvider.state);

    return Material(
        child: Scaffold(
            appBar: AppBar(
              flexibleSpace: Container(
                  decoration:
                      BoxDecoration(gradient: styles.panelDecoration.gradient)),
              title: DefaultTextStyle(
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(color: styles.themeColors.onBrandText),
                  child: Row(children: const [
                    Text("ConfApp"),
                  ])),
            ),
            extendBodyBehindAppBar: true,
            extendBody: true,
            body: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        height: 515,
                        width: 455,
                        foregroundDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                                color: styles.themeColors.brandDarker,
                                width: 2)),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Image.asset("assets/images/logo.png",
                                    width: 250, height: 82.26),
                              ),
                              Container(
                                  child: Form(
                                    key: _emKey,
                                    child: TextFormField(
                                      controller: emailController,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(100)
                                      ],
                                      decoration: const InputDecoration(
                                          labelText: "Email",
                                          hintText: "example@examp.ru"),
                                      validator: (email) => email!.isEmpty ||
                                              email.contains('@') == false
                                          ? 'Enter a valid email'
                                          : null,
                                    ),
                                  ),
                                  width: 400,
                                  padding: const EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 20.0)),
                              Container(
                                child: Form(
                                    key: _pasKey,
                                    child: TextFormField(
                                      controller: passwordController,
                                      obscureText: !visib,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(100)
                                      ],
                                      decoration: InputDecoration(
                                          labelText: "Password",
                                          hintText: "Enter password",
                                          suffixIcon: IconButton(
                                            icon: visib
                                                ? const Icon(
                                                    Icons.visibility_off)
                                                : const Icon(Icons.visibility),
                                            onPressed: () {
                                              visib = !visib;
                                              setState(() {});
                                            },
                                          )),
                                      validator: (password) =>
                                          password != null &&
                                                  password.length < 8
                                              ? 'Enter min 8 character'
                                              : null,
                                    )),
                                width: 400,
                                padding: const EdgeInsets.fromLTRB(
                                    10.0, 20.0, 10.0, 10.0),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        10.0, 70.0, 47.0, 0.0),
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 24, horizontal: 24),
                                            textStyle: Theme.of(context)
                                                .textTheme
                                                .bodyText2!
                                                .copyWith(
                                                    color: styles.themeColors
                                                        .background)),
                                        onPressed: () {
                                          if (_emKey.currentState!.validate() &&
                                              _pasKey.currentState!
                                                  .validate()) {
                                            currentUser.state = User(
                                                login: emailController.text,
                                                id: Common.uuid.v4());
                                          }
                                        },
                                        child: const Text("Log In")),
                                  )
                                ],
                              )
                            ]))
                  ]),
            )));
  }
}
