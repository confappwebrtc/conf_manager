///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Speaker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Speaker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastname')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'firstname')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'middlename')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..a<$core.List<$core.int>>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'photo', $pb.PbFieldType.OY)
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'city')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gender')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dob')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'about')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'job')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'company')
    ..aOB(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'invited')
    ..aOS(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOS(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'telegram')
    ..aOS(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'vk')
    ..hasRequiredFields = false
  ;

  Speaker._() : super();
  factory Speaker({
    $core.String? id,
    $core.String? lastname,
    $core.String? firstname,
    $core.String? middlename,
    $core.String? nickname,
    $core.List<$core.int>? photo,
    $core.String? city,
    $core.String? gender,
    $core.String? dob,
    $core.String? about,
    $core.String? job,
    $core.String? company,
    $core.bool? invited,
    $core.String? status,
    $core.String? email,
    $core.String? phone,
    $core.String? telegram,
    $core.String? vk,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (lastname != null) {
      _result.lastname = lastname;
    }
    if (firstname != null) {
      _result.firstname = firstname;
    }
    if (middlename != null) {
      _result.middlename = middlename;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (photo != null) {
      _result.photo = photo;
    }
    if (city != null) {
      _result.city = city;
    }
    if (gender != null) {
      _result.gender = gender;
    }
    if (dob != null) {
      _result.dob = dob;
    }
    if (about != null) {
      _result.about = about;
    }
    if (job != null) {
      _result.job = job;
    }
    if (company != null) {
      _result.company = company;
    }
    if (invited != null) {
      _result.invited = invited;
    }
    if (status != null) {
      _result.status = status;
    }
    if (email != null) {
      _result.email = email;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (telegram != null) {
      _result.telegram = telegram;
    }
    if (vk != null) {
      _result.vk = vk;
    }
    return _result;
  }
  factory Speaker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Speaker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Speaker clone() => Speaker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Speaker copyWith(void Function(Speaker) updates) => super.copyWith((message) => updates(message as Speaker)) as Speaker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Speaker create() => Speaker._();
  Speaker createEmptyInstance() => create();
  static $pb.PbList<Speaker> createRepeated() => $pb.PbList<Speaker>();
  @$core.pragma('dart2js:noInline')
  static Speaker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Speaker>(create);
  static Speaker? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get lastname => $_getSZ(1);
  @$pb.TagNumber(2)
  set lastname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLastname() => $_has(1);
  @$pb.TagNumber(2)
  void clearLastname() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get firstname => $_getSZ(2);
  @$pb.TagNumber(3)
  set firstname($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFirstname() => $_has(2);
  @$pb.TagNumber(3)
  void clearFirstname() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get middlename => $_getSZ(3);
  @$pb.TagNumber(4)
  set middlename($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMiddlename() => $_has(3);
  @$pb.TagNumber(4)
  void clearMiddlename() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get nickname => $_getSZ(4);
  @$pb.TagNumber(5)
  set nickname($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasNickname() => $_has(4);
  @$pb.TagNumber(5)
  void clearNickname() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<$core.int> get photo => $_getN(5);
  @$pb.TagNumber(6)
  set photo($core.List<$core.int> v) { $_setBytes(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPhoto() => $_has(5);
  @$pb.TagNumber(6)
  void clearPhoto() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get city => $_getSZ(6);
  @$pb.TagNumber(7)
  set city($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasCity() => $_has(6);
  @$pb.TagNumber(7)
  void clearCity() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get gender => $_getSZ(7);
  @$pb.TagNumber(8)
  set gender($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasGender() => $_has(7);
  @$pb.TagNumber(8)
  void clearGender() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get dob => $_getSZ(8);
  @$pb.TagNumber(9)
  set dob($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasDob() => $_has(8);
  @$pb.TagNumber(9)
  void clearDob() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get about => $_getSZ(9);
  @$pb.TagNumber(10)
  set about($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasAbout() => $_has(9);
  @$pb.TagNumber(10)
  void clearAbout() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get job => $_getSZ(10);
  @$pb.TagNumber(11)
  set job($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasJob() => $_has(10);
  @$pb.TagNumber(11)
  void clearJob() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get company => $_getSZ(11);
  @$pb.TagNumber(12)
  set company($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasCompany() => $_has(11);
  @$pb.TagNumber(12)
  void clearCompany() => clearField(12);

  @$pb.TagNumber(13)
  $core.bool get invited => $_getBF(12);
  @$pb.TagNumber(13)
  set invited($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasInvited() => $_has(12);
  @$pb.TagNumber(13)
  void clearInvited() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get status => $_getSZ(13);
  @$pb.TagNumber(14)
  set status($core.String v) { $_setString(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasStatus() => $_has(13);
  @$pb.TagNumber(14)
  void clearStatus() => clearField(14);

  @$pb.TagNumber(15)
  $core.String get email => $_getSZ(14);
  @$pb.TagNumber(15)
  set email($core.String v) { $_setString(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasEmail() => $_has(14);
  @$pb.TagNumber(15)
  void clearEmail() => clearField(15);

  @$pb.TagNumber(16)
  $core.String get phone => $_getSZ(15);
  @$pb.TagNumber(16)
  set phone($core.String v) { $_setString(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasPhone() => $_has(15);
  @$pb.TagNumber(16)
  void clearPhone() => clearField(16);

  @$pb.TagNumber(17)
  $core.String get telegram => $_getSZ(16);
  @$pb.TagNumber(17)
  set telegram($core.String v) { $_setString(16, v); }
  @$pb.TagNumber(17)
  $core.bool hasTelegram() => $_has(16);
  @$pb.TagNumber(17)
  void clearTelegram() => clearField(17);

  @$pb.TagNumber(18)
  $core.String get vk => $_getSZ(17);
  @$pb.TagNumber(18)
  set vk($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasVk() => $_has(17);
  @$pb.TagNumber(18)
  void clearVk() => clearField(18);
}

class Promo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Promo', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'about')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..a<$core.List<$core.int>>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'photo', $pb.PbFieldType.OY)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOB(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'invited')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'telegram')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'vk')
    ..hasRequiredFields = false
  ;

  Promo._() : super();
  factory Promo({
    $core.String? id,
    $core.String? title,
    $core.String? about,
    $core.String? nickname,
    $core.List<$core.int>? photo,
    $core.String? address,
    $core.bool? invited,
    $core.String? status,
    $core.String? email,
    $core.String? phone,
    $core.String? telegram,
    $core.String? vk,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (title != null) {
      _result.title = title;
    }
    if (about != null) {
      _result.about = about;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (photo != null) {
      _result.photo = photo;
    }
    if (address != null) {
      _result.address = address;
    }
    if (invited != null) {
      _result.invited = invited;
    }
    if (status != null) {
      _result.status = status;
    }
    if (email != null) {
      _result.email = email;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (telegram != null) {
      _result.telegram = telegram;
    }
    if (vk != null) {
      _result.vk = vk;
    }
    return _result;
  }
  factory Promo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Promo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Promo clone() => Promo()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Promo copyWith(void Function(Promo) updates) => super.copyWith((message) => updates(message as Promo)) as Promo; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Promo create() => Promo._();
  Promo createEmptyInstance() => create();
  static $pb.PbList<Promo> createRepeated() => $pb.PbList<Promo>();
  @$core.pragma('dart2js:noInline')
  static Promo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Promo>(create);
  static Promo? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get about => $_getSZ(2);
  @$pb.TagNumber(3)
  set about($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAbout() => $_has(2);
  @$pb.TagNumber(3)
  void clearAbout() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get nickname => $_getSZ(3);
  @$pb.TagNumber(4)
  set nickname($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNickname() => $_has(3);
  @$pb.TagNumber(4)
  void clearNickname() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.int> get photo => $_getN(4);
  @$pb.TagNumber(5)
  set photo($core.List<$core.int> v) { $_setBytes(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPhoto() => $_has(4);
  @$pb.TagNumber(5)
  void clearPhoto() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get address => $_getSZ(5);
  @$pb.TagNumber(6)
  set address($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAddress() => $_has(5);
  @$pb.TagNumber(6)
  void clearAddress() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get invited => $_getBF(6);
  @$pb.TagNumber(7)
  set invited($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasInvited() => $_has(6);
  @$pb.TagNumber(7)
  void clearInvited() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get status => $_getSZ(7);
  @$pb.TagNumber(8)
  set status($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasStatus() => $_has(7);
  @$pb.TagNumber(8)
  void clearStatus() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get email => $_getSZ(8);
  @$pb.TagNumber(9)
  set email($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasEmail() => $_has(8);
  @$pb.TagNumber(9)
  void clearEmail() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get phone => $_getSZ(9);
  @$pb.TagNumber(10)
  set phone($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPhone() => $_has(9);
  @$pb.TagNumber(10)
  void clearPhone() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get telegram => $_getSZ(10);
  @$pb.TagNumber(11)
  set telegram($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasTelegram() => $_has(10);
  @$pb.TagNumber(11)
  void clearTelegram() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get vk => $_getSZ(11);
  @$pb.TagNumber(12)
  set vk($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasVk() => $_has(11);
  @$pb.TagNumber(12)
  void clearVk() => clearField(12);
}

class Guest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Guest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'photo', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  Guest._() : super();
  factory Guest({
    $core.String? id,
    $core.String? nickname,
    $core.List<$core.int>? photo,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (photo != null) {
      _result.photo = photo;
    }
    return _result;
  }
  factory Guest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Guest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Guest clone() => Guest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Guest copyWith(void Function(Guest) updates) => super.copyWith((message) => updates(message as Guest)) as Guest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Guest create() => Guest._();
  Guest createEmptyInstance() => create();
  static $pb.PbList<Guest> createRepeated() => $pb.PbList<Guest>();
  @$core.pragma('dart2js:noInline')
  static Guest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Guest>(create);
  static Guest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nickname => $_getSZ(1);
  @$pb.TagNumber(2)
  set nickname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNickname() => $_has(1);
  @$pb.TagNumber(2)
  void clearNickname() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get photo => $_getN(2);
  @$pb.TagNumber(3)
  set photo($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPhoto() => $_has(2);
  @$pb.TagNumber(3)
  void clearPhoto() => clearField(3);
}

class Permission extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Permission', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  Permission._() : super();
  factory Permission({
    $core.String? id,
    $core.String? name,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory Permission.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Permission.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Permission clone() => Permission()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Permission copyWith(void Function(Permission) updates) => super.copyWith((message) => updates(message as Permission)) as Permission; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Permission create() => Permission._();
  Permission createEmptyInstance() => create();
  static $pb.PbList<Permission> createRepeated() => $pb.PbList<Permission>();
  @$core.pragma('dart2js:noInline')
  static Permission getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Permission>(create);
  static Permission? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class Role extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Role', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  Role._() : super();
  factory Role({
    $core.String? id,
    $core.String? name,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory Role.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Role.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Role clone() => Role()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Role copyWith(void Function(Role) updates) => super.copyWith((message) => updates(message as Role)) as Role; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Role create() => Role._();
  Role createEmptyInstance() => create();
  static $pb.PbList<Role> createRepeated() => $pb.PbList<Role>();
  @$core.pragma('dart2js:noInline')
  static Role getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Role>(create);
  static Role? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class Roles extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Roles', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..pc<Role>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roles', $pb.PbFieldType.PM, subBuilder: Role.create)
    ..hasRequiredFields = false
  ;

  Roles._() : super();
  factory Roles({
    $core.Iterable<Role>? roles,
  }) {
    final _result = create();
    if (roles != null) {
      _result.roles.addAll(roles);
    }
    return _result;
  }
  factory Roles.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Roles.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Roles clone() => Roles()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Roles copyWith(void Function(Roles) updates) => super.copyWith((message) => updates(message as Roles)) as Roles; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Roles create() => Roles._();
  Roles createEmptyInstance() => create();
  static $pb.PbList<Roles> createRepeated() => $pb.PbList<Roles>();
  @$core.pragma('dart2js:noInline')
  static Roles getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Roles>(create);
  static Roles? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Role> get roles => $_getList(0);
}

class Permissions extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Permissions', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..pc<Permission>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permissions', $pb.PbFieldType.PM, subBuilder: Permission.create)
    ..hasRequiredFields = false
  ;

  Permissions._() : super();
  factory Permissions({
    $core.Iterable<Permission>? permissions,
  }) {
    final _result = create();
    if (permissions != null) {
      _result.permissions.addAll(permissions);
    }
    return _result;
  }
  factory Permissions.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Permissions.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Permissions clone() => Permissions()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Permissions copyWith(void Function(Permissions) updates) => super.copyWith((message) => updates(message as Permissions)) as Permissions; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Permissions create() => Permissions._();
  Permissions createEmptyInstance() => create();
  static $pb.PbList<Permissions> createRepeated() => $pb.PbList<Permissions>();
  @$core.pragma('dart2js:noInline')
  static Permissions getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Permissions>(create);
  static Permissions? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Permission> get permissions => $_getList(0);
}

enum User_Type {
  speaker, 
  promo, 
  guest, 
  notSet
}

class User extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, User_Type> _User_TypeByTag = {
    4 : User_Type.speaker,
    5 : User_Type.promo,
    6 : User_Type.guest,
    0 : User_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'User', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..oo(0, [4, 5, 6])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'login')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..aOM<Speaker>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'speaker', subBuilder: Speaker.create)
    ..aOM<Promo>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'promo', subBuilder: Promo.create)
    ..aOM<Guest>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'guest', subBuilder: Guest.create)
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'role')
    ..pc<Permission>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permissions', $pb.PbFieldType.PM, subBuilder: Permission.create)
    ..hasRequiredFields = false
  ;

  User._() : super();
  factory User({
    $core.String? id,
    $core.String? login,
    $core.String? password,
    Speaker? speaker,
    Promo? promo,
    Guest? guest,
    $core.String? role,
    $core.Iterable<Permission>? permissions,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (login != null) {
      _result.login = login;
    }
    if (password != null) {
      _result.password = password;
    }
    if (speaker != null) {
      _result.speaker = speaker;
    }
    if (promo != null) {
      _result.promo = promo;
    }
    if (guest != null) {
      _result.guest = guest;
    }
    if (role != null) {
      _result.role = role;
    }
    if (permissions != null) {
      _result.permissions.addAll(permissions);
    }
    return _result;
  }
  factory User.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  User clone() => User()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  User copyWith(void Function(User) updates) => super.copyWith((message) => updates(message as User)) as User; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User create() => User._();
  User createEmptyInstance() => create();
  static $pb.PbList<User> createRepeated() => $pb.PbList<User>();
  @$core.pragma('dart2js:noInline')
  static User getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User>(create);
  static User? _defaultInstance;

  User_Type whichType() => _User_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get login => $_getSZ(1);
  @$pb.TagNumber(2)
  set login($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLogin() => $_has(1);
  @$pb.TagNumber(2)
  void clearLogin() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  Speaker get speaker => $_getN(3);
  @$pb.TagNumber(4)
  set speaker(Speaker v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasSpeaker() => $_has(3);
  @$pb.TagNumber(4)
  void clearSpeaker() => clearField(4);
  @$pb.TagNumber(4)
  Speaker ensureSpeaker() => $_ensure(3);

  @$pb.TagNumber(5)
  Promo get promo => $_getN(4);
  @$pb.TagNumber(5)
  set promo(Promo v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasPromo() => $_has(4);
  @$pb.TagNumber(5)
  void clearPromo() => clearField(5);
  @$pb.TagNumber(5)
  Promo ensurePromo() => $_ensure(4);

  @$pb.TagNumber(6)
  Guest get guest => $_getN(5);
  @$pb.TagNumber(6)
  set guest(Guest v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasGuest() => $_has(5);
  @$pb.TagNumber(6)
  void clearGuest() => clearField(6);
  @$pb.TagNumber(6)
  Guest ensureGuest() => $_ensure(5);

  @$pb.TagNumber(7)
  $core.String get role => $_getSZ(6);
  @$pb.TagNumber(7)
  set role($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasRole() => $_has(6);
  @$pb.TagNumber(7)
  void clearRole() => clearField(7);

  @$pb.TagNumber(8)
  $core.List<Permission> get permissions => $_getList(7);
}

enum User_row_Type {
  speakerId, 
  promoId, 
  guestId, 
  notSet
}

class User_row extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, User_row_Type> _User_row_TypeByTag = {
    4 : User_row_Type.speakerId,
    5 : User_row_Type.promoId,
    6 : User_row_Type.guestId,
    0 : User_row_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'User_row', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'conferencepackage'), createEmptyInstance: create)
    ..oo(0, [4, 5, 6])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'login')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'speakerId')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'promoId')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'guestId')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'role')
    ..pc<Permission>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permissions', $pb.PbFieldType.PM, subBuilder: Permission.create)
    ..hasRequiredFields = false
  ;

  User_row._() : super();
  factory User_row({
    $core.String? id,
    $core.String? login,
    $core.String? password,
    $core.String? speakerId,
    $core.String? promoId,
    $core.String? guestId,
    $core.String? role,
    $core.Iterable<Permission>? permissions,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (login != null) {
      _result.login = login;
    }
    if (password != null) {
      _result.password = password;
    }
    if (speakerId != null) {
      _result.speakerId = speakerId;
    }
    if (promoId != null) {
      _result.promoId = promoId;
    }
    if (guestId != null) {
      _result.guestId = guestId;
    }
    if (role != null) {
      _result.role = role;
    }
    if (permissions != null) {
      _result.permissions.addAll(permissions);
    }
    return _result;
  }
  factory User_row.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User_row.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  User_row clone() => User_row()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  User_row copyWith(void Function(User_row) updates) => super.copyWith((message) => updates(message as User_row)) as User_row; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User_row create() => User_row._();
  User_row createEmptyInstance() => create();
  static $pb.PbList<User_row> createRepeated() => $pb.PbList<User_row>();
  @$core.pragma('dart2js:noInline')
  static User_row getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User_row>(create);
  static User_row? _defaultInstance;

  User_row_Type whichType() => _User_row_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get login => $_getSZ(1);
  @$pb.TagNumber(2)
  set login($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLogin() => $_has(1);
  @$pb.TagNumber(2)
  void clearLogin() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get speakerId => $_getSZ(3);
  @$pb.TagNumber(4)
  set speakerId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSpeakerId() => $_has(3);
  @$pb.TagNumber(4)
  void clearSpeakerId() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get promoId => $_getSZ(4);
  @$pb.TagNumber(5)
  set promoId($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPromoId() => $_has(4);
  @$pb.TagNumber(5)
  void clearPromoId() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get guestId => $_getSZ(5);
  @$pb.TagNumber(6)
  set guestId($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasGuestId() => $_has(5);
  @$pb.TagNumber(6)
  void clearGuestId() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get role => $_getSZ(6);
  @$pb.TagNumber(7)
  set role($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasRole() => $_has(6);
  @$pb.TagNumber(7)
  void clearRole() => clearField(7);

  @$pb.TagNumber(8)
  $core.List<Permission> get permissions => $_getList(7);
}

