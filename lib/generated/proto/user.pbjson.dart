///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use speakerDescriptor instead')
const Speaker$json = const {
  '1': 'Speaker',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'lastname', '3': 2, '4': 1, '5': 9, '10': 'lastname'},
    const {'1': 'firstname', '3': 3, '4': 1, '5': 9, '10': 'firstname'},
    const {'1': 'middlename', '3': 4, '4': 1, '5': 9, '10': 'middlename'},
    const {'1': 'nickname', '3': 5, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'photo', '3': 6, '4': 1, '5': 12, '10': 'photo'},
    const {'1': 'city', '3': 7, '4': 1, '5': 9, '10': 'city'},
    const {'1': 'gender', '3': 8, '4': 1, '5': 9, '10': 'gender'},
    const {'1': 'dob', '3': 9, '4': 1, '5': 9, '10': 'dob'},
    const {'1': 'about', '3': 10, '4': 1, '5': 9, '10': 'about'},
    const {'1': 'job', '3': 11, '4': 1, '5': 9, '10': 'job'},
    const {'1': 'company', '3': 12, '4': 1, '5': 9, '10': 'company'},
    const {'1': 'invited', '3': 13, '4': 1, '5': 8, '10': 'invited'},
    const {'1': 'status', '3': 14, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'email', '3': 15, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone', '3': 16, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'telegram', '3': 17, '4': 1, '5': 9, '10': 'telegram'},
    const {'1': 'vk', '3': 18, '4': 1, '5': 9, '10': 'vk'},
  ],
};

/// Descriptor for `Speaker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List speakerDescriptor = $convert.base64Decode('CgdTcGVha2VyEg4KAmlkGAEgASgJUgJpZBIaCghsYXN0bmFtZRgCIAEoCVIIbGFzdG5hbWUSHAoJZmlyc3RuYW1lGAMgASgJUglmaXJzdG5hbWUSHgoKbWlkZGxlbmFtZRgEIAEoCVIKbWlkZGxlbmFtZRIaCghuaWNrbmFtZRgFIAEoCVIIbmlja25hbWUSFAoFcGhvdG8YBiABKAxSBXBob3RvEhIKBGNpdHkYByABKAlSBGNpdHkSFgoGZ2VuZGVyGAggASgJUgZnZW5kZXISEAoDZG9iGAkgASgJUgNkb2ISFAoFYWJvdXQYCiABKAlSBWFib3V0EhAKA2pvYhgLIAEoCVIDam9iEhgKB2NvbXBhbnkYDCABKAlSB2NvbXBhbnkSGAoHaW52aXRlZBgNIAEoCFIHaW52aXRlZBIWCgZzdGF0dXMYDiABKAlSBnN0YXR1cxIUCgVlbWFpbBgPIAEoCVIFZW1haWwSFAoFcGhvbmUYECABKAlSBXBob25lEhoKCHRlbGVncmFtGBEgASgJUgh0ZWxlZ3JhbRIOCgJ2axgSIAEoCVICdms=');
@$core.Deprecated('Use promoDescriptor instead')
const Promo$json = const {
  '1': 'Promo',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'about', '3': 3, '4': 1, '5': 9, '10': 'about'},
    const {'1': 'nickname', '3': 4, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'photo', '3': 5, '4': 1, '5': 12, '10': 'photo'},
    const {'1': 'address', '3': 6, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'invited', '3': 7, '4': 1, '5': 8, '10': 'invited'},
    const {'1': 'status', '3': 8, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'email', '3': 9, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone', '3': 10, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'telegram', '3': 11, '4': 1, '5': 9, '10': 'telegram'},
    const {'1': 'vk', '3': 12, '4': 1, '5': 9, '10': 'vk'},
  ],
};

/// Descriptor for `Promo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List promoDescriptor = $convert.base64Decode('CgVQcm9tbxIOCgJpZBgBIAEoCVICaWQSFAoFdGl0bGUYAiABKAlSBXRpdGxlEhQKBWFib3V0GAMgASgJUgVhYm91dBIaCghuaWNrbmFtZRgEIAEoCVIIbmlja25hbWUSFAoFcGhvdG8YBSABKAxSBXBob3RvEhgKB2FkZHJlc3MYBiABKAlSB2FkZHJlc3MSGAoHaW52aXRlZBgHIAEoCFIHaW52aXRlZBIWCgZzdGF0dXMYCCABKAlSBnN0YXR1cxIUCgVlbWFpbBgJIAEoCVIFZW1haWwSFAoFcGhvbmUYCiABKAlSBXBob25lEhoKCHRlbGVncmFtGAsgASgJUgh0ZWxlZ3JhbRIOCgJ2axgMIAEoCVICdms=');
@$core.Deprecated('Use guestDescriptor instead')
const Guest$json = const {
  '1': 'Guest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'nickname', '3': 2, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'photo', '3': 3, '4': 1, '5': 12, '10': 'photo'},
  ],
};

/// Descriptor for `Guest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List guestDescriptor = $convert.base64Decode('CgVHdWVzdBIOCgJpZBgBIAEoCVICaWQSGgoIbmlja25hbWUYAiABKAlSCG5pY2tuYW1lEhQKBXBob3RvGAMgASgMUgVwaG90bw==');
@$core.Deprecated('Use permissionDescriptor instead')
const Permission$json = const {
  '1': 'Permission',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `Permission`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List permissionDescriptor = $convert.base64Decode('CgpQZXJtaXNzaW9uEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1l');
@$core.Deprecated('Use roleDescriptor instead')
const Role$json = const {
  '1': 'Role',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `Role`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roleDescriptor = $convert.base64Decode('CgRSb2xlEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1l');
@$core.Deprecated('Use rolesDescriptor instead')
const Roles$json = const {
  '1': 'Roles',
  '2': const [
    const {'1': 'roles', '3': 1, '4': 3, '5': 11, '6': '.conferencepackage.Role', '10': 'roles'},
  ],
};

/// Descriptor for `Roles`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rolesDescriptor = $convert.base64Decode('CgVSb2xlcxItCgVyb2xlcxgBIAMoCzIXLmNvbmZlcmVuY2VwYWNrYWdlLlJvbGVSBXJvbGVz');
@$core.Deprecated('Use permissionsDescriptor instead')
const Permissions$json = const {
  '1': 'Permissions',
  '2': const [
    const {'1': 'permissions', '3': 1, '4': 3, '5': 11, '6': '.conferencepackage.Permission', '10': 'permissions'},
  ],
};

/// Descriptor for `Permissions`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List permissionsDescriptor = $convert.base64Decode('CgtQZXJtaXNzaW9ucxI/CgtwZXJtaXNzaW9ucxgBIAMoCzIdLmNvbmZlcmVuY2VwYWNrYWdlLlBlcm1pc3Npb25SC3Blcm1pc3Npb25z');
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'login', '3': 2, '4': 1, '5': 9, '10': 'login'},
    const {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'speaker', '3': 4, '4': 1, '5': 11, '6': '.conferencepackage.Speaker', '9': 0, '10': 'speaker'},
    const {'1': 'promo', '3': 5, '4': 1, '5': 11, '6': '.conferencepackage.Promo', '9': 0, '10': 'promo'},
    const {'1': 'guest', '3': 6, '4': 1, '5': 11, '6': '.conferencepackage.Guest', '9': 0, '10': 'guest'},
    const {'1': 'role', '3': 7, '4': 1, '5': 9, '10': 'role'},
    const {'1': 'permissions', '3': 8, '4': 3, '5': 11, '6': '.conferencepackage.Permission', '10': 'permissions'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEg4KAmlkGAEgASgJUgJpZBIUCgVsb2dpbhgCIAEoCVIFbG9naW4SGgoIcGFzc3dvcmQYAyABKAlSCHBhc3N3b3JkEjYKB3NwZWFrZXIYBCABKAsyGi5jb25mZXJlbmNlcGFja2FnZS5TcGVha2VySABSB3NwZWFrZXISMAoFcHJvbW8YBSABKAsyGC5jb25mZXJlbmNlcGFja2FnZS5Qcm9tb0gAUgVwcm9tbxIwCgVndWVzdBgGIAEoCzIYLmNvbmZlcmVuY2VwYWNrYWdlLkd1ZXN0SABSBWd1ZXN0EhIKBHJvbGUYByABKAlSBHJvbGUSPwoLcGVybWlzc2lvbnMYCCADKAsyHS5jb25mZXJlbmNlcGFja2FnZS5QZXJtaXNzaW9uUgtwZXJtaXNzaW9uc0IGCgR0eXBl');
@$core.Deprecated('Use user_rowDescriptor instead')
const User_row$json = const {
  '1': 'User_row',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'login', '3': 2, '4': 1, '5': 9, '10': 'login'},
    const {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'speaker_id', '3': 4, '4': 1, '5': 9, '9': 0, '10': 'speakerId'},
    const {'1': 'promo_id', '3': 5, '4': 1, '5': 9, '9': 0, '10': 'promoId'},
    const {'1': 'guest_id', '3': 6, '4': 1, '5': 9, '9': 0, '10': 'guestId'},
    const {'1': 'role', '3': 7, '4': 1, '5': 9, '10': 'role'},
    const {'1': 'permissions', '3': 8, '4': 3, '5': 11, '6': '.conferencepackage.Permission', '10': 'permissions'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `User_row`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List user_rowDescriptor = $convert.base64Decode('CghVc2VyX3JvdxIOCgJpZBgBIAEoCVICaWQSFAoFbG9naW4YAiABKAlSBWxvZ2luEhoKCHBhc3N3b3JkGAMgASgJUghwYXNzd29yZBIfCgpzcGVha2VyX2lkGAQgASgJSABSCXNwZWFrZXJJZBIbCghwcm9tb19pZBgFIAEoCUgAUgdwcm9tb0lkEhsKCGd1ZXN0X2lkGAYgASgJSABSB2d1ZXN0SWQSEgoEcm9sZRgHIAEoCVIEcm9sZRI/CgtwZXJtaXNzaW9ucxgIIAMoCzIdLmNvbmZlcmVuY2VwYWNrYWdlLlBlcm1pc3Npb25SC3Blcm1pc3Npb25zQgYKBHR5cGU=');
